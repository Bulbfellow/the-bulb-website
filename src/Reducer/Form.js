const data = []

const Forminfo = (state = data, { type, payload }) => {
    return {
        ...state,
        [type]: payload
    }
}

export default Forminfo