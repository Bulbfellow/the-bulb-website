import React from 'react';
import { Api } from '../Const/Api'
import Axios from 'axios'

export const Fileupload = (file) => {
    return new Promise((resolve, reject) => {
        let formData = new FormData()
        formData.append(
            'file',
            file
        )
        Axios.post(Api.BaseUrl + '/api/file-upload', formData)
            .then(resp => {
                if (resp.status === 200) {
                    resolve(resp.data)
                }
                else {
                    reject("please try again")
                }
            }).catch(err => {
                reject("Something Went Wrong Please Try Again latter")
            })
    })
}

export const Statelist = () => {
    return new Promise((resolve, reject) => {
        Axios.get(Api.BaseUrl + "/api/states")
            .then(resp => {
                // console.log("state list", resp)
                if (resp.status === 200) {
                    resolve(resp.data.data)
                }
                else {
                    reject("please try again")
                }
            })
    })
}

export const Citylist = (id) => {
    return new Promise((resolve, reject) => {
        Axios.get(Api.BaseUrl+'/api/states/' + id)
        .then(resp => {
                // console.log("city  list", resp)
                if (resp.status === 200) {
                    resolve(resp.data.data)
                }
                else {
                    reject("please try again")
                }
            })
    })
}


