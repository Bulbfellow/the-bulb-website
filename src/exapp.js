import React, { Component } from 'react';
import Switch from 'react-router-dom/Switch';
import './App.css';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import rootReducers from './Reducer'
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import Navigation from './Container/Navigation';
import './Assets/css/style.css'
import './Assets/css/fonts.css'
// import './Assets/css/_slick.scss'
// import './Assets/css/_slickTheme.scss'
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

const store = createStore(rootReducers, {},
  composeWithDevTools(
    applyMiddleware(thunk))
)

class ExApp extends Component {
  render() {
    return (
      <Provider store={store}>
        <Navigation />
      </Provider>
    )
  }
}

export default ExApp;

