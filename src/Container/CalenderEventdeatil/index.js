import React, { Component } from 'react'
// import eventimage from '../../Assets/img/ai.png'

export default class Calenderevent extends Component {

 


    render() {
        return (
            <div className="grid-view grid-events">
                <hr />
                <div className="grid-events-inner py-4">
                    <div className="card mb-4 flex-row align-items-center">
                        <div className="card-left events-photo">
                            <img src={eventimage} alt="Events Image" className="img-fluid" />
                        </div>
                        <div className="card-right events-desc">
                            <h4 className="event-title mb-0"><a href="#">Dinner: Community meetup Lagos</a></h4>
                            <p className="event-des">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt, sed diam nonumy eirmod tempor invidunt</p>
                            <div className="events-dateTime"><span className="date">Tuesday, October 10</span> . <span className="time">6:00 - 7:30PM</span></div>
                            <div className="events-place"><span>Nylah's,</span> <span className="events-address">61 Queen Street Alagomeji Yaba, Yaba, Lagos, Nigeria</span></div>
                            <div className="register-event mt-3 text-right">
                                <button className="btn btn-outline-warning">Register</button>
                            </div>
                        </div>
                    </div>

                    <div className="card mb-4 flex-row align-items-center">
                        <div className="card-left events-photo">
                            <img src={eventimage} alt="Events Image" className="img-fluid" />
                        </div>
                        <div className="card-right events-desc">
                            <h4 className="event-title mb-0"><a href="#">Dinner: Community meetup Lagos</a></h4>
                            <p className="event-des">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt</p>
                            <div className="events-dateTime"><span className="date">Tuesday, October 10</span> . <span className="time">6:00 - 7:30PM</span></div>
                            <div className="events-place"><span>Nylah's,</span> <span className="events-address">61 Queen Street Alagomeji Yaba, Yaba, Lagos, Nigeria</span></div>
                            <div className="register-event mt-3 text-right">
                                <button className="btn btn-outline-warning">Register</button>
                            </div>
                        </div>
                    </div>

                    <div className="card mb-4 flex-row align-items-center">
                        <div className="card-left events-photo">
                            <img src={eventimage} alt="Events Image" className="img-fluid" />
                        </div>
                        <div className="card-right events-desc">
                            <h4 className="event-title mb-0"><a href="#">Dinner: Community meetup Lagos</a></h4>
                            <p className="event-des">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt</p>
                            <div className="events-dateTime"><span className="date">Tuesday, October 10</span> . <span className="time">6:00 - 7:30PM</span></div>
                            <div className="events-place"><span>Nylah's,</span> <span className="events-address">61 Queen Street Alagomeji Yaba, Yaba, Lagos, Nigeria</span></div>
                            <div className="register-event mt-3 text-right">
                                <button className="btn btn-outline-warning">Register</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
