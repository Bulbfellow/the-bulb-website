import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Typewriter from 'typewriter-effect';

const Dashboard = ({ headerv, clasname, clasnamebtn, btnclassadd, traningselect, callcalenderselect }) => {
    useEffect(() => {
        document.getElementById("navTop").style.background = "transparent";
    }, [])

    useEffect(() => {
        const script = document.createElement("script");
        script.src = "./js/animate-bg.js";
        script.async = true;
        document.head.appendChild(script);
    })

    useEffect(() => {
        headerv && closeAll()
    }, [headerv])

    const [setindidual, setSetindidual] = useState(false)
    const [corporate, setCorporate] = useState(false)
    const [ngo, setNgo] = useState(false)
    const [closemenubar, setClosemenubar] = useState(true)

    function addClass() {
        clasname(closemenubar)
        clasnamebtn(closemenubar)
        btnclassadd(closemenubar)
    }

    function closeAll() {
        setSetindidual(false)
        setCorporate(false)
        setNgo(false)
    }

    function inover() {
        setSetindidual(false)
        setCorporate(false)
        setNgo(false)
    }

    function onmouseiver() {
        setSetindidual(true)
        setCorporate(false)
        setNgo(false)
    }

    function corporatehover() {
        setCorporate(false)
        setSetindidual(false)
        setNgo(false)
        setCorporate(true)
    }

    function ngomousehover() {
        setNgo(true)
        setCorporate(false)
        setSetindidual(false)
    }

    let indi = ''
    let indiarrow = ''
    if (setindidual === true) {
        indi = " individual-bg-slide";
        indiarrow = " indiarrow"
    }
    else {
        indi = "";
        indiarrow = ""
    }

    let corporateclass = ''
    let coparrow = ''
    if (corporate === true) {
        corporateclass = " corporation-bg-slide";
        coparrow = " copratearrow"
    }
    else {
        corporateclass = "";
        coparrow = ""
    }

    let ngoclass = ''
    let ngoarrow = ''
    if (ngo === true) {
        ngoclass = " ngo-bg-slide";
        ngoarrow = " ngoratearrow";

    }
    else {
        ngoclass = "";
        ngoarrow = ""
    }
    return (
        <>
            <div id="particles-js" className="uk-position-fixed"></div>
            <div className="slider position-relative" onClick={() => addClass(setClosemenubar(!closemenubar))}>
                <div className="slider_inner">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-md-7">
                                <div className="slider-content">
                                    <h1 id="sliderref">
                                        Innovate, Incubate &amp; Accelerate your <span className="w-1px d-inline-block">&nbsp;</span>
                                        <Typewriter
                                            options={
                                                {
                                                    strings: ['Tech Idea', 'Startup', 'Company', 'IT Solutions'],
                                                    autoStart: true,
                                                    loop: true,
                                                    cursor: '',
                                                    wrapperClassName: "typing"
                                                }}
                                        />
                                    </h1>
                                </div>
                            </div>
                            <div className="col-md-5">
                                <img src='https://thebulb.africa/Images/homepage/bulb_light.svg' alt="bulb" className="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row m-0 slider-bottom">
                    <div className={"slider-bottom-inner" + indiarrow} onMouseEnter={onmouseiver}  >
                        <img src='https://thebulb.africa/Images/homepage/arrow-down.png' alt="Down Arrow" className="img-fluid" />
                        <h3>Individuals</h3>
                    </div>
                    <div className={"slider-bottom-inner" + coparrow} onMouseEnter={corporatehover}  >
                        <img src='https://thebulb.africa/Images/homepage/arrow-down.png' alt="Down Arrow" className="img-fluid" />
                        <h3>Corporations</h3>
                    </div>
                    <div className={"slider-bottom-inner" + ngoarrow} onMouseEnter={ngomousehover}  >
                        <img src='https://thebulb.africa/Images/homepage/arrow-down.png' alt="Down Arrow" className="img-fluid" />
                        <h3>Partner with us</h3>
                    </div>
                </div>

                <div className="row m-0 d-flex align-items-center position-absolute slider-hover-content" >
                    <div className={"slider-hover-inner indviduals-bg" + indi} onMouseLeave={inover} >
                        <ul>
                            <li><Link to="/startups">Startups</Link></li>
                            <li><Link to="/become-developer">Become a developer</Link></li>
                            <li><Link to="/community">Community</Link></li>
                            <li><Link to="/training" onClick={() => traningselect(false)}>Training</Link></li>
                            <li><Link to="/careers">Careers</Link></li>
                            <li><Link to="/newsfeed"
                                onClick={() => callcalenderselect(true)}
                            >
                                Events</Link></li>
                        </ul>
                    </div>
                    <div className={"slider-hover-inner corporation-bg" + corporateclass} onMouseLeave={inover} >
                        <ul>
                            <li><Link to="/hire-developer">Hire developers</Link></li>
                            <li><Link to="/partner-with-us">Partner with us</Link></li>
                            <li><Link to="/community">Community</Link></li>
                            <li><Link to="/training" onClick={() => traningselect(true)}>Training</Link></li>
                            <li>
                                <Link to="/co-workspace">Workspace</Link>
                            </li>
                            <li><Link to="/newsfeed" onClick={() => callcalenderselect(true)}>Events</Link></li>
                        </ul>
                    </div>
                    <div className={"slider-hover-inner ngo-bg" + ngoclass} onMouseLeave={inover} >
                        <ul>
                            <li><Link to="/partner-with-us">Corporate</Link></li>
                            <li><Link to="/">NGO</Link></li>
                            <li><Link to="/">Sponsorship request</Link></li>
                            <li><Link to="/training" onClick={() => traningselect(true)}>Training</Link></li>
                            <li><Link to="/schedule-a-visit">Schedule a visit</Link></li>
                            {/* <li><Link to="/newsfeed" onClick={() => callcalenderselect(true)}>Events</Link></li> */}
                        </ul>
                    </div>
                </div>
            </div>
        </>
    )
}
const mapStateToProps = (state) => {
    return {
        navnarclassaddremove: state.classesadd
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        clasname: d => dispatch({ type: "DIV_CLASS_ADD", payload: d }),
        clasnamebtn: d => dispatch({ type: "BTN_EXPAND_ADD", payload: d }),
        btnclassadd: d => dispatch({ type: "BTN_CLASS_ADD", payload: d })
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
