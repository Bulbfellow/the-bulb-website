import React, { useState, useEffect } from 'react'
// import press from '../../../Assets/img/press.png'
// import aiimg from '../../../Assets/img/ai.png'
// import { recentpost } from '../../../Const/data'
import Newsfeedcard from '../../../Components/Newsfeedcard'
import { NewsfeedTagGallery } from '../../../Action/newsfeedAction'
import { connect } from 'react-redux'
import ImageGallery from 'react-image-gallery';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Dateconvert } from '../../../Const/data'

// import Image1 from '../../../Assets/img/tech-2.png';
// import Image2 from '../../../Assets/img/ai.png';
// import Image3 from '../../../Assets/img/media.png';
// import Image4 from '../../../Assets/img/press.png';
// import Image5 from '../../../Assets/img/tech.png';
// import Image6 from '../../../Assets/img/tech-1.png';

// const images = [
//     {
//         original: Image1,
//         thumbnail: Image1,
//     },
//     {
//         original: Image2,
//         thumbnail: Image2,
//     },
// ]

const Gallary = ({ newsFeedGalleryDispatch, newsFeedGalleryState }) => {
    console.log("newsFeedGalleryState", newsFeedGalleryState)
    const [show, setShow] = useState(false)
    const [particulargallery, setParticulargallery] = useState([])
    const [loader, setLoader] = useState(true)
    const [errormessage, setErrormessage] = useState('')
    const [dataLimit, setDataLimit] = useState(9)

    useEffect(() => {
        newsFeedGalleryDispatch(dataLimit)
    }, [dataLimit])

    const pageCount = newsFeedGalleryState.paging.pageCount;

    useEffect(() => {
        setLoader(newsFeedGalleryState.loading)
        setErrormessage(newsFeedGalleryState.error)
    }, [newsFeedGalleryState.loading, newsFeedGalleryState.error])

    const handelShowMore = () => {
        if (pageCount > 1) {
            let addLimit = dataLimit + 9
            setDataLimit(addLimit)
        }
    }

    let images = []
    {
        show && show && particulargallery.image.map((tag) => {
            let data = {
                original: tag,
                thumbnail: tag
            }
            images.push(data)
        })
    }

    let eventlist = []
    {
        newsFeedGalleryState.resp.length > 0 && newsFeedGalleryState.resp.map((tag) => {
            let data = {
                image: tag.image_urls,
                title: tag.title,
                description: tag.description,
                date: tag.date,
                venue: tag.venue
            }
            eventlist.push(data)
        })
    }

    return (
        <>
            {show && show === true ?
                <div className="gallerytab-inner">
                    <div className="container-fluid position-relative">
                        <div className="close-galleryTab position-relative" style={{ cursor: 'pointer' }}>
                            <button onClick={() => setShow(false)} className="btn"><i className="fas fa-times"></i></button>
                        </div>
                        <div className="row pt-5 pt-md-0">
                            <div className="col-md-1"></div>
                            <div className="col-md-5">
                                <div className="gallery-small">
                                    <ImageGallery items={show && show && images}
                                        showBullets={false}
                                        showNav={true}
                                        autoPlay={true}
                                        showFullscreenButton={false}
                                        showPlayButton={false}
                                        thumbnailPosition="bottom"
                                        showIndex={true}
                                    />
                                </div>
                            </div>
                            <div className="col-md-1"></div>
                            <div className="col-md-4">
                                <div className="gallery-images-count mt-md-5">
                                    <p>1 / {particulargallery.image.length}</p>
                                </div>
                                <div className="gallery-inner-dtl">
                                    <h3>{particulargallery.title}</h3>
                                    <span className="date">{Dateconvert(particulargallery.date, 3)}</span>
                                    <p className="place mb-4"> <span>{particulargallery.venue}</span></p>
                                    <span dangerouslySetInnerHTML={{ __html: particulargallery.description }}></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                :
                <div className="feeds-inner-tab">
                    {loader && loader && <CircularProgress />}
                    {errormessage}
                    {eventlist.length === 0 && <p>No post yet
</p> }

                    <div className="row">

                        {eventlist.length > 0 && eventlist.map((gallery, sr_no) => {
                            let data = gallery.description;
                            let sub = data.substring(0, 145) + "...";
                            console.log("gallery", gallery)
                            return (
                                <div key={sr_no} className="col-md-4 col-sm-6" style={{ cursor: 'pointer' }}>
                                    <div className="feed-section">
                                        <div className="feed-section" onClick={() => { setShow(!show); setParticulargallery(gallery) }}>
                                            <div className="feedImg">
                                                <img src={gallery.image[0]} alt="Recent" className="img-fluid" />
                                            </div>
                                            <div className="feedContent">
                                                <h3>{gallery.title}</h3>

                                                {data.length < 145 ?
                                                    <p className="line-clamp" dangerouslySetInnerHTML={{ __html: gallery.description }}></p>
                                                    : <p className="line-clamp" dangerouslySetInnerHTML={{ __html: sub }}></p>}
                                                <p className="date">{Dateconvert(gallery.date, 4)} </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}


                        {/* <Newsfeedcard
                            list={eventlist}
                            gallery={true}
                            onClick={() => setShow(!show)}
                            singleinfo={singleinfo}
                        /> */}
                    </div>
                    {(pageCount > 1) ?
                        <button className="btn btn-warning mx-auto d-table" onClick={() => handelShowMore(pageCount)}>Show More</button> : null
                    }
                </div>
            }
        </>
    )
}
const mapStateToProps = (state) => {
    return {
        newsFeedGalleryState: state.NewsFeedGallery
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        newsFeedGalleryDispatch: (dataLimit) => { dispatch(NewsfeedTagGallery(dataLimit)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Gallary)
