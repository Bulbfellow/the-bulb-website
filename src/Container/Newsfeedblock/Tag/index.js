import React, { useState, useEffect } from 'react'
import Newsfeedcard from '../../../Components/Newsfeedcard'
import { connect } from 'react-redux'
import { NewsFeedTag } from '../../../Action/newsfeedAction'
import CircularProgress from '@material-ui/core/CircularProgress';

const Tag = ({ newsFeedTagState, newsFeedTagDispatch }) => {
    console.log(newsFeedTagState)
    const [filter, setFilter] = useState(false)
    const [value, setValue] = useState('ALL')
    const [loader, setLoader] = useState(true)
    const [errormessage, setErrormessage] = useState('')
    const [dataLimit, setDataLimit] = useState(9)
    const [search, setSearch] = useState('ALL')



    const pageCount = newsFeedTagState.paging.pageCount;
    let data = {
        search,
        dataLimit
    }
    useEffect(() => {
        setLoader(newsFeedTagState.loading)
        setErrormessage(newsFeedTagState.error)
    }, [newsFeedTagState.loading, newsFeedTagState.error])

    useEffect(() => {
        newsFeedTagDispatch(data)
    }, [dataLimit, search])

    function filterdata(name) {
        setValue(name)
        setFilter(true)
    }

    let eventlist = []
    {
        newsFeedTagState.resp.length > 0 && newsFeedTagState.resp.map((tag,i) => {
            let data = {
                key: i,
                image: tag.image_url,
                title: tag.title,
                description: tag.description,
                date: tag.rowmodifieddatetime,
                type: tag.tag_type,
                id: tag.sr_no,
                // category: tag.link,
            }
            eventlist.push(data)

        })
    }

    const handelShowMore = () => {
        if (pageCount > 1) {
            let addLimit = dataLimit + 9
            setDataLimit(addLimit)
        }
    }


    return (
        <div className="feeds-inner-tab">
            <div className="tags-filter">
                <ul className="pl-0 mb-4 d-flex justify-content-center justify-content-md-start tags-filter-inner align-items-center flex-wrap">
                    <li><span className={search === 'ALL' && "active-tag"} onClick={() => { setFilter(false); setSearch('ALL') }}>ALL</span></li>
                    <li><span className={search === 'AI' && "active-tag"} onClick={() => setSearch('AI')}>AI</span></li>
                    <li><span className={search === 'Tech' && "active-tag"} onClick={() => setSearch('Tech')}>Tech</span></li>
                    <li><span className={search === 'Media' && "active-tag"} onClick={() => setSearch('Media')}>Media</span></li>
                    <li><span className={search === 'Event' && "active-tag"} onClick={() => setSearch('Event')}>Events</span></li>
                    <li><span className={search === 'Opinion' && "active-tag"} onClick={() => setSearch('Opinion')}>Opinion</span></li>
                </ul>
            </div>
            <div className="row">
                {loader && loader && <CircularProgress />}
                {errormessage}
                {eventlist.length === 0 && <p>No post yet
</p> }
                <Newsfeedcard
                    list={eventlist}
                    cursor={true}
                    link={"/tag-description"}
                />
            </div>
            {(pageCount > 1) ?
                <button className="btn btn-warning mx-auto d-table" onClick={() => handelShowMore(pageCount)}>Show More</button> : null
            }
        </div>
    )
}
const mapStateToProps = (state) => {
    return {
        newsFeedTagState: state.NewsFeedTag
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        newsFeedTagDispatch: (data) => { dispatch(NewsFeedTag(data)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Tag)

