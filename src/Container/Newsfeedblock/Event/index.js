import React, { useState, useEffect } from 'react';
import Calenderevent from '../../../Components/Calenderevent'
import { calenderdata } from '../../../Const/data'
import BigCalendar from 'react-big-calendar'
import moment from 'moment'
import { connect } from 'react-redux'
import { NewsFeedEvent, NewsFeedEventCalender } from '../../../Action/newsfeedAction'
import CircularProgress from '@material-ui/core/CircularProgress';
import { Dateconvert } from '../../../Const/data'

const localizer = BigCalendar.momentLocalizer(moment)



const Event = ({ onClick, calenderselect, newsFeedEventDispatch, newsFeedEventState, NewsFeedEventCalender, NewsFeedEventCalenderState }) => {
   
    let Eventss = []
    {
        NewsFeedEventCalenderState.resp.length > 0 && NewsFeedEventCalenderState.resp.map((event,i) => {
          console.log("event",event)
            let data = {
                key: i,
                title: event.title,
                start: event.date,
                end: event.date,
                resource: event.description,
                url: event.link,
                venue: event.venue
            }
            Eventss.push(data)
        })
    }


    // const Eventss = [{
    //     title: 'Dinner: Community meetup Lagos',
    //     start: new Date(2019, 11, 5),
    //     end: new Date(2019, 11, 5),
    //     resource: "61 Queen Street Alagomeji Yaba, Yaba, Lagos, Nigeria",
    //     url: 'https://www.eventbrite.com/e/lagos-fintech-week-tickets -56974352823'
    // },
    // {
    //     title: 'Testing',
    //     start: new Date(2020, 03, 04),
    //     end: new Date(2020, 03, 05),
    //     resource: "dsadsdsdsdsdd",
    //     url: 'google.com'
    // }]

    const [show, setShow] = useState(calenderselect === true ? false : true)
    const [filter, setFilter] = useState(false)
    const [topic, setTopic] = useState('')
    const [date, setDate] = useState('All dates')
    const [current, setCurrent] = useState(new Date(''))
    const [activetab, setActivetab] = useState(calenderselect === true ? 'calenderlistview' : 'calenderview')
    const [eventpopup, setEventpopup] = useState([])
    const [showpopup, setShowpopup] = useState(false)
    const [loader, setLoader] = useState(true)
    const [errormessage, setErrormessage] = useState('')
    const [dataLimit, setDataLimit] = useState(10)
    const [search, setSearch] = useState('ALL')

    let data = {
        dataLimit,
        search
    }
    useEffect(() => {
        newsFeedEventDispatch(data)
    }, [dataLimit, search])

    useEffect(() => {
        NewsFeedEventCalender()
    }, [])

    const pageCount = newsFeedEventState.paging.pageCount;

    useEffect(() => {
        setLoader(newsFeedEventState.loading)
        setErrormessage(newsFeedEventState.error)
    }, [newsFeedEventState.loading, newsFeedEventState.error])

    const handelShowMore = () => {
        if (pageCount > 1) {
            let addLimit = dataLimit + 10
            setDataLimit(addLimit)
        }
    }
    function Selectopic(name) {
        // alert(name)
        setSearch(name)
        setFilter(true)
    }

    function onSelectEvent(e) {
        setEventpopup(e)
        setShowpopup(true)
    }

    function dd(event, start, end, isSelected) {
        var style = {
            backgroundColor: '#10152C',
            borderRadius: '8px',
            color: '#FDBF14',
            border: '0px',
            fontSize: '10px'
        };
        return {
            style: style
        };
    }

    const onNavigate = (focusDate, flipUnit, prevOrNext) => {

        const now = new Date();
        const nowNum = now.getDate();
        const nextWeekToday = moment().add(7, "day").toDate();
        const nextWeekTodayNum = nextWeekToday.getDate();

        if (prevOrNext === "NEXTs"
            && current.getDate() === nowNum) {
            setCurrent(nextWeekToday)

        } else if (prevOrNext === "PRsEV"
            && current.getDate() === nextWeekTodayNum) {
            setCurrent(now)
        }
    }
    const messages = {
        previous: <i className="fa fa-arrow-left" aria-hidden="true"></i>,
        next: <i className="fa fa-arrow-right" aria-hidden="true"></i>
    };

    function Event({ event }) {
        // console.log('eventevent',event)
        return (
            <span>
                <strong>
                    {event.title}
                </strong>
                {event.desc && (':  ' + event.desc)}
            </span>
        )
    }
    let eventlist = []
    {
        newsFeedEventState.resp.length > 0 && newsFeedEventState.resp.map((event, k) => {
    
            if (event.event_post == "EVENT" || event.event_post == "ALL") {
                let data = {
                    key: k,
                    image: event.image_url,
                    title: event.title,
                    des: event.description,
                    date: event.date,
                    time: event.time,
                    address: event.venue,
                    registerlink: event.link,
                    type: event.event_type
                }
                eventlist.push(data)
            }
        })
    }


    return (
        <div className="calendar-tab">
            <div className="calendar-filter mb-4 d-flex align-items-center">
                <div className="sort_topic mr-4 sort-cal d-flex align-items-center">
                    <label htmlFor="sortTopic" className="mb-0 pr-2 pr-md-3 d-none d-block-md">Sort by topic: </label>
                    <div className="dropdown">
                        <button className="btn btn-outline-warning dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {topic === '' ? "All Topic" : topic} <i className="fas fa-angle-down"></i>
                        </button>
                        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton" style={{ cursor: 'pointer' }}>
                            <li onClick={() => { setFilter(false); setSearch('All') }} className={`dropdown-item ${search === 'All Topic' && "active"}`}>All Topic</li>
                            <li onClick={() => setSearch('Conference')} className={`dropdown-item ${search === 'Conference' && "active"}`}>Conference</li>
                            <li onClick={() => setSearch('Contest')} className={`dropdown-item ${search === 'Contest' && "active"}`}>Contest</li>
                            <li onClick={() => setSearch('Hackathons')} className={`dropdown-item ${search === 'Hackathons' && "active"}`}>Hackathons</li>
                            <li onClick={() => setSearch('Workshop')} className={`dropdown-item ${search === 'Workshop' && "active"}`}>Workshop</li>
                        </div>
                    </div>
                </div>
                <div className="sort_date mr-sm-4 sort-cal d-flex align-items-center">
                    <label htmlFor="sortDate" className="mb-0 pr-2 pr-md-3 d-none d-block-md">Sort by Date: </label>
                    <div className="dropdown">
                        <button className="btn btn-outline-warning dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {date} <i className="fas fa-angle-down"></i>
                        </button>
                        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton" style={{ cursor: 'pointer' }}>
                            <li onClick={() => setDate('All dates')} className={`dropdown-item ${date === 'All Topic' && "active"}`}>All dates</li>
                            <li onClick={() => setDate('Today')} className={`dropdown-item ${date === 'Today' && "active"}`}>Today</li>
                            <li onClick={() => setDate('This week')} className={`dropdown-item ${date === 'This week' && "active"}`}>This week</li>
                            <li onClick={() => setDate('Next week')} className={`dropdown-item ${date === 'Next week' && "active"}`}>Next week</li>
                            <li onClick={() => setDate('This month')} className={`dropdown-item ${date === 'This month' && "active"}`}>This month</li>
                        </div>
                    </div>
                </div>
                <div className="past-events sort-cal mr-3 ml-md-auto">
                    <button onClick={onClick} className="btn btn-outline-warning">Past events</button>
                </div>
                <div className="bygrid sort-cal">
                    <button className={`btn mr-2 ${activetab === 'calenderview' && "active"}`} onClick={() => { setShow(true); setActivetab('calenderview') }}><i className="fas fa-calendar"></i></button>
                    <button className={`btn mr-2 ${activetab === 'calenderlistview' && "active"}`} onClick={() => { setShow(false); setActivetab('calenderlistview') }}><i className="fas fa-list"></i></button>
                </div>
            </div>
            {
                show && show ?
                    <>
                        <BigCalendar
                            localizer={localizer}
                            events={Eventss}
                            selectable
                            popup
                            style={{ height: 500 }}
                            messages={messages} // new
                            step={60}

                            drilldownView="agenda"
                            eventPropGetter={(event) => dd(event)}
                            views={{ month: true, week: true, day: true }}
                            onSelectEvent={event => onSelectEvent(event)}
                            onNavigate={onNavigate}
                            // onView={(e) => test(e)}
                            onClick={() => setShowpopup(false)}
                            components={{
                                event: Event,
                            }}
                        />
                        {showpopup && showpopup &&
                            <div className="eventpopup">
                                <h1>{eventpopup.title}</h1>
                                <p className="event_date">{Dateconvert(eventpopup.start, 4)}</p>
                                <div className="event-location  position-relative">
                                    <i className="fas fa-map-marker position-absolute"></i>
                                    {/* <h5>{eventpopup.resource}</h5> */}
                                    <p>{eventpopup.venue}</p>
                                    <p dangerouslySetInnerHTML={{ __html: eventpopup.resource }}></p>

                                </div>
                                <div className="click-register">
                                    <h5>Click here to register</h5>
                                    <a target="_blank" rel="noopener noreferrer" href={eventpopup.url}>{eventpopup.url}</a>
                                </div>
                                <button className="btn btn-warning btn-sm d-table px-4 text-white ml-auto mt-3" onClick={() => setShowpopup(false)}>close</button>

                            </div>

                        }

                    </>
                    :
                    // "No post yet"
                    <>
                        {loader && loader && <CircularProgress />}
                        {errormessage}
                        {eventlist.length === 0 && <p>No post yet
</p> }
                        <Calenderevent
                            list={eventlist}
                        />
                        {(pageCount > 1) ?
                            <button className="btn btn-warning mx-auto d-table" onClick={() => handelShowMore(pageCount)}>Show More</button> : null
                        }
                    </>
            }
        </div>
    )
}
const mapStateToProps = (state) => {
    return {
        newsFeedEventState: state.NewsFeedEvent,
        NewsFeedEventCalenderState: state.NewsFeedEventCalender
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        NewsFeedEventCalender: () => { dispatch(NewsFeedEventCalender()) },
        newsFeedEventDispatch: (data) => { dispatch(NewsFeedEvent(data)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Event)