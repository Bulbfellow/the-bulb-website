import React, { useEffect, useState } from 'react'
import Newsfeedcard from '../../../Components/Newsfeedcard'
import { connect } from 'react-redux'
import { NewsfeedTagPress } from '../../../Action/newsfeedAction'
import CircularProgress from '@material-ui/core/CircularProgress';

const Press = ({ newsFeedPressDispatch, newsFeedPressState }) => {

    const [loader, setLoader] = useState(false)
    const [errormessage, setErrormessage] = useState('')
    const [dataLimit, setDataLimit] = useState(9)

    useEffect(() => {
        newsFeedPressDispatch(dataLimit)
    }, [dataLimit])

    const pageCount = newsFeedPressState.paging.pageCount;

    useEffect(() => {
        setLoader(newsFeedPressState.loading)
        setErrormessage(newsFeedPressState.error)
    }, [newsFeedPressState.loading, newsFeedPressState.error])

    let eventlist = []
    {
        newsFeedPressState.resp.length > 0 && newsFeedPressState.resp.map((tag,i) => {
            let data = {
                key: i,
                image: tag.image_url,
                title: tag.title,
                description: tag.description,
                date: tag.rowmodifieddatetime,
                id: tag.sr_no
            }
            eventlist.push(data)
        })
    }

    const handelShowMore = () => {
        if (pageCount > 1) {
            let addLimit = dataLimit + 9
            setDataLimit(addLimit)
        }
    }

    return (
        <>
            <div className="feeds-inner-tab">
                <div className="row">
                    {loader && loader && <div className="loadercuss"><CircularProgress /></div>}
                    {errormessage}
                    {eventlist.length === 0 && <p>No post yet
</p> }
                    <Newsfeedcard
                        list={eventlist}
                        link={"/press-description"}
                    />
                </div>
                { (pageCount > 1)?
                <button className="btn btn-warning mx-auto d-table" onClick={() => handelShowMore(pageCount)}>Show More</button> : null
            }
            </div>
        </>
    )
}
const mapStateToProps = (state) => {
    return {
        newsFeedPressState: state.NewsFeedPress
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        newsFeedPressDispatch: (dataLimit) => { dispatch(NewsfeedTagPress(dataLimit)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Press)