import React from 'react'
import { recentpost } from '../../../Const/data'
import Newsfeedcard from '../../../Components/Newsfeedcard'
import Youtubmodal from '../../../Components/Youtubemodal'
// import live1 from '../../../Assets/img/events-live.png'

const Live = ({ }) => {
  return (
    <>
      <div className="feeds-inner-tab">
        <div className="live-video live-tab">
          <div className="live-video-inner col-sm-7 col-md-5 col-lg-4">
            <div className="video-time">56 : 00</div>
            <h2>The Bulb conference 2019 California USA</h2>
            <button className="btn btn-warning px-5 text-white" data-toggle="modal" data-target="#videoYoutube">Watch live</button>
          </div>
        </div>
        <div className="related-live-videos pt-5">
          <div className="row">
            <div className="col-sm-6 col-md-4">
              <div className="rel-live-inner mb-4 d-inline-block w-100">
                <a href="#" data-toggle="modal" data-target="#videoYoutube">
                  <div className="live-thumb position-relative mb-2">
                    {/* <img alt="image" src={live1} alt="Live Events" className="img-fluid" /> */}
                    <div className="video-time position-absolute">56:00</div>
                  </div>
                  <div className="live-video-dtl">
                    <h4>The title of the event goes here</h4>
                    <p className="live-date">October 22, 2019 <span className="category">Tech</span></p>
                  </div>
                </a>
              </div>
            </div>
            <div className="col-sm-6 col-md-4">
              <div className="rel-live-inner mb-4 d-inline-block w-100">
                <a href="#" data-toggle="modal" data-target="#videoYoutube">
                  <div className="live-thumb position-relative mb-2">
                    {/* <img alt="image" src={live1} alt="Live Events" className="img-fluid" /> */}
                    <div className="video-time position-absolute">56:00</div>
                  </div>
                  <div className="live-video-dtl">
                    <h4>The title of the event goes here</h4>
                    <p className="live-date">October 22, 2019 <span className="category">Tech</span></p>
                  </div>
                </a>
              </div>
            </div>
            <div className="col-sm-6 col-md-4">
              <div className="rel-live-inner mb-4 d-inline-block w-100">
                <a href="#" data-toggle="modal" data-target="#videoYoutube">
                  <div className="live-thumb position-relative mb-2">
                    {/* <img alt="image" src={live1} alt="Live Events" className="img-fluid" /> */}
                    <div className="video-time position-absolute">56:00</div>
                  </div>
                  <div className="live-video-dtl">
                    <h4>The title of the event goes here</h4>
                    <p className="live-date">October 22, 2019 <span className="category">Tech</span></p>
                  </div>
                </a>
              </div>
            </div>
            <div className="col-sm-6 col-md-4">
              <div className="rel-live-inner mb-4 d-inline-block w-100">
                <a href="#" data-toggle="modal" data-target="#videoYoutube">
                  <div className="live-thumb position-relative mb-2">
                    {/* <img alt="image" src={live1} alt="Live Events" className="img-fluid" /> */}
                    <div className="video-time position-absolute">56:00</div>
                  </div>
                  <div className="live-video-dtl">
                    <h4>The title of the event goes here</h4>
                    <p className="live-date">October 22, 2019 <span className="category">Tech</span></p>
                  </div>
                </a>
              </div>
            </div>
            <div className="col-sm-6 col-md-4">
              <div className="rel-live-inner mb-4 d-inline-block w-100">
                <a href="#" data-toggle="modal" data-target="#videoYoutube">
                  <div className="live-thumb position-relative mb-2">
                    {/* <img alt="image" src={live1} alt="Live Events" className="img-fluid" /> */}
                    <div className="video-time position-absolute">56:00</div>
                  </div>
                  <div className="live-video-dtl">
                    <h4>The title of the event goes here</h4>
                    <p className="live-date">October 22, 2019 <span className="category">Tech</span></p>
                  </div>
                </a>
              </div>
            </div>
            <div className="col-sm-6 col-md-4">
              <div className="rel-live-inner mb-4 d-inline-block w-100">
                <a href="#" data-toggle="modal" data-target="#videoYoutube">
                  <div className="live-thumb position-relative mb-2">
                    {/* <img alt="image" src={live1} alt="Live Events" className="img-fluid" /> */}
                    <div className="video-time position-absolute">56:00</div>
                  </div>
                  <div className="live-video-dtl">
                    <h4>The title of the event goes here</h4>
                    <p className="live-date">October 22, 2019 <span className="category">Tech</span></p>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      
      <div className="modal fade" id="videoYoutube" tabindex="-1" role="dialog" aria-labelledby="videoYoutubeTitle" aria-hidden="true">
        <div className="modal-dialog modal-dialog-scrollable modal-dialog-centered max-width-95" role="document">
          <button type="button" className="close position-absolute" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <div className="modal-content w-100">
            {/* <div className="modal-header">
            </div> */}
            <div className="modal-body p-0">
              <iframe title="youtube" width="100%" height="610px" src="https://www.youtube.com/embed/9xwazD5SyVg" frameBorder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
export default Live