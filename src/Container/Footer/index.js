import React, { useState } from 'react'
import footerlogo from '../../Assets/img/footer-logo.png'
import { Link } from 'react-router-dom'
import './style.css'
import Axios from 'axios'
import Dynamicalert from '../../Components/Dynamicmodal'
import {Api} from '../../Const/Api'

const Footer = () => {
	const [email, setemail] = useState('')
	const [openmodal, setOpenmodal] = useState(false)
	const [message, setMessage] = useState('')

	const Emailsubmit = (e) => {
		setemail(e)
		Axios.post(Api.BaseUrl+'/api/subscriber', {
			type : "desk",
			email: email
		})
			.then(res => {
				console.log("newsletter", res)
				setOpenmodal(true)
				setMessage(res.data.message)
				setemail('')
			},
			)
	}
	const handleClose = () => {
		setOpenmodal(false)
	};
	return (
		<footer className="footer">
			<div className="container">
				<div className="row">
					<div className="col-md-2 col-6">
						<img src={footerlogo} alt="The Bulb" className="img-fluid" />
					</div>
					<div className="col-md-2 col-6">
						<div className="footer-links">
							<h5><b>Explore</b></h5>
							<ul className="list-inline">
								<li><Link to="/aboutus">About</Link></li>
								<li><Link to="/coworkspace">Co-work space</Link></li>
								<li><Link to="/thehub">The hub</Link></li>
								<li><Link to="/newsfeed">News feed</Link></li>
								<li><Link to="/careers">Careers</Link></li>
							</ul>
						</div>
					</div>
					<div className="col-md-2 col-6">
						<div className="footer-links">
							<h5><b>Help</b></h5>
							<ul className="list-inline">
								<li><Link to="/faqs">FAQs</Link></li>
								<li><Link to="/resources">Resources</Link></li>
								 {/* <li><Link to="/privacy_policy">Privacy policy</Link></li> */}
                            <li><Link to="/terms-and-condition">Terms &amp; Conditions</Link></li>
							</ul>
						</div>
					</div>
					<div className="col-md-3 col-6">
						<div className="footer-links address">
							<h5><b>Contact us</b></h5>
							<ul className="list-inline">
								<li>39 Ikorodu Road, Jibowu, Yaba, Lagos </li>
								<li><a href="tel:+2347008342852">+234 700 834 2852</a></li>
								<li><a href="mailto:info@thebulb.com">info@thebulb.africa</a></li>
							</ul>
						</div>
					</div>
					<div className="col-md-3 col-12">
						<div className="footer-links address">
							<h5><b>Connect</b></h5>
							<div className="subscribe-form">
								<label htmlFor="">Sign up for our newsletters</label>
								<div className="input-group">
									<input type="text" className="form-control" placeholder="Email address" onChange={(e) => setemail(e.target.value)} aria-label="Email address" aria-describedby="basic-addon2" />
									<div className="input-group-append">
										<button className="btn btn-outline-secondary" onClick={(e) => Emailsubmit(e.target.value)} type="button">Subscribe</button>
									</div>
								</div>
							</div>
						</div><i className="fab fa-linkedin"></i>
						<div className="footer-social">
							<a href="https://www.facebook.com/TheBulbAfrica/?_rdc=1&_rdr" target="_blank" className="text-white pr-4"><i className="fab fa-facebook-f fa-lg"></i></a>
							<a href="https://twitter.com/thebulbafrica" target="_blank" className="text-white pr-4"><i className="fab fa-twitter fa-lg"></i></a>
							<a href="https://www.instagram.com/thebulb.africa/" target="_blank" className="text-white pr-4"><i className="fab fa-instagram fa-lg"></i></a>
							<a href="https://www.linkedin.com/company/64547766/" target="_blank" className="text-white"><i className="fab fa-linkedin fa-lg"></i></a>
						</div>
					</div>
					<div className="col-12 text-center">
						<p className="copyright">Copyright © 2019 Thebulb All rights reserved</p>
					</div>
				</div>
			</div>
			{/* <Snackbar open={openmodal} autoHideDuration={2000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success">
                    {message}  </Alert>
            </Snackbar> */}
			<Dynamicalert
				open={openmodal}
				message={message}
				close={handleClose}
			/>
		</footer>

	)

}
export default Footer
