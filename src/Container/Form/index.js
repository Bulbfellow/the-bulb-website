import React, { Component } from 'react'
import { connect } from 'react-redux'
class Form extends Component {

    validate = () => {
        console.log('this.props.formValue', this.props.formValue)
        const formdata = this.props.formValue

        var output = Object.entries(formdata).map(([key, value]) => ({key,value}));

        console.log('output',output)
        // formdata.map((user) => {
        //     return console.log('user', user)
        // })
    }

    submitForm = (e) => {
        e.preventDefault()
        const valid = this.validate()
        if (valid) {
            alert('dasds')
        }
        alert("submit")
    }
    render() {
        return (
            <div className="reqest-tour submit-ideaForm mt-md-3 py-3 py-sm-5 mb-5">
                <div className="container">
                    <form onSubmit={this.submitForm}>
                        <div className="card">
                            <div className="card-body">
                                <div className="request-heading">
                                    <h3>{this.props.Title}</h3>
                                    <p className="d-none d-md-block">{this.props.Description}</p>
                                </div>
                                {this.props.children}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        formValue: state.Form
    }
}
const mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Form)
