import React from 'react'
import { NavLink, Link } from 'react-router-dom'
import { connect } from 'react-redux'
const Header = ({ closeAll, headerv, callcalenderselect }) => {
    return (
        <nav className="navbar fixed-top navbar-expand-md justify-content-between navbar-light p-0 px-3" id="navTop">
            <Link to="/" className="navbar-brand">
                <img src='https://thebulb.africa/Images/homepage/logo.png' alt="logo" onClick={() => closeAll(!headerv)} />
            </Link>
            <button type="button" className="btn btn-danger dropdown-toggle d-md-none" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="dropdown-menu nav-top-cus">
                <ul className="navbar-nav ml-auto">
                   
                    <li className="nav-item">
                        <NavLink activeClassName="active" to="/aboutus" className="nav-link">
                            About
                                </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink activeClassName="active" to="/co-workspace" className="nav-link">
                            Co-workspace
                                </NavLink>
                        
                    </li>
                    <li className="nav-item">
                        <NavLink activeClassName="active" to="/thehub" className="nav-link">
                            The Hub
                                </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink onClick={() => callcalenderselect(false)} activeClassName="active" to="/newsfeed" className="nav-link">
                            News Feed
                                </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink activeClassName="active" to="/contact" className="nav-link">
                            Contact
                                </NavLink>
                    </li>
                </ul>
            </div>
        </nav>

    )

}
const mapStateToProps = (state) => {
    return {
        navnarclassaddremove: state.classesadd,
        navnarclassbtn: state.classesadd,
        navnarclassbtnexpand: state.classesadd
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);
