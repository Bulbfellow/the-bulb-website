import React, { useState } from 'react'
import Header from '../Header'
import Dashboard from '../Dashboard'
import { Route } from 'react-router-dom';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import Thehub from '../../Pages/Thehub'
import Newsfeed from '../../Pages/Newsfeed'
import Aboutpage from '../../Pages/About'
import Coworkspace from '../../Pages/Coworkspace'
import Contactpage from '../../Pages/Contact'
import Formcontact from '../../Container/Formcontact'
import Recentpost from '../../Container/Recent'
// import Testing from '../../Container/Testing'
import Traning from '../../Pages/Commonpages/Trainings'
import Startups from '../../Pages/Commonpages/Startups'
import Schedulewithus from '../../Pages/Commonpages/Schedulewithus'
import Partnerwithus from '../../Pages/Commonpages/Partnerwithus'
import Hireadeveloper from '../../Pages/Commonpages/Hiredeveloper'
import Community from '../../Pages/Commonpages/Community'
import Career from '../../Pages/Commonpages/Careers'
import Becomeadeveloper from '../../Pages/Commonpages/BecomeAdeveloper'
import Resourses from '../../Pages/Footerpage/Resourses'
import Termsandcondition from '../../Pages/Footerpage/Termsandcondions'
import Privacypolicy from '../../Pages/Footerpage/Privacypolicy'
import Faqs from '../../Pages/Footerpage/Faq'
import CareerExpended from '../../Pages/Commonpages/CareerExpended'
import SubmitIdea from '../../Pages/Commonpages/SubmitIdea'
import ApplyIncubator from '../../Pages/Commonpages/ApplyIncubator'
import NotFound from '../../Pages/Commonpages/NotFound'
import Pressdesciption from '../../Pages/Newsfeedpage/Pressdescription'
import Tagdescription from '../../Pages/Newsfeedpage/Tagdescription'
import Commingsoon from '../../Container/CommingSoon'
import Nojobfound from '../../Container/Nojobfound'

const Navigation = () => {
    const [state, setstate] = useState(false)
    const [changetoggle, setChangetoggle] = useState(false)
    const [calenderchange, setCalenderchange] = useState(false)

    return (
        <Router>
            <Header closeAll={setstate} headerv={state} callcalenderselect={setCalenderchange}
            />
            <Switch>
                <Route exact path='/' component={() => <Dashboard
                    headerv={state}
                    // changetype={changetoggle}
                    traningselect={setChangetoggle}
                    // calenderselect={calenderchange}
                    callcalenderselect={setCalenderchange}
                />}
                />
                {/* <Route exact path={`/:id`} component={CareerExpended} /> */}
                <Route path='/thehub' component={Thehub} />
                <Route path='/newsfeed' component={() => <Newsfeed calenderselect={calenderchange} />} />
                <Route path='/aboutus' component={Aboutpage} />
                <Route path={`/press-description/:id`} component={Pressdesciption} />
                <Route path={`/tag-description/:id`} component={Tagdescription} />
                <Route path='/co-workspace' component={Coworkspace} />
                <Route path='/contact' component={Contactpage} />
                <Route path='/request-a-tour' component={Formcontact} />
                <Route path='/recent' component={Recentpost} />
                <Route path='/startups' component={Startups} />
                <Route path='/become-developer' component={Becomeadeveloper} />
                <Route path='/community' component={Commingsoon} />
                <Route path='/training' component={() => <Traning changetype={changetoggle} />} />
                <Route exact path='/careers' component={Career} />
                <Route path='/hire-developer' component={Hireadeveloper} />
                <Route path='/partner-with-us' component={Partnerwithus} />
                <Route path='/schedule-a-visit' component={() => <Schedulewithus />} />
                <Route path='/resources' component={Commingsoon} />
                <Route path='/privacy-policy' component={Privacypolicy} />
                <Route path='/terms-and-condition' component={Termsandcondition} />
                <Route path='/faqs' component={Commingsoon} />
                <Route path={`/jobs/:id`} component={CareerExpended} />
                <Route path='/submit-an-idea' component={SubmitIdea} />
                <Route path='/apply-incubator' component={ApplyIncubator} />
                <Route path="/no-job-found" component={Nojobfound}/>
                <Route exact path='*' component={NotFound} />
            </Switch>
        </Router>
    )
}

export default (Navigation)