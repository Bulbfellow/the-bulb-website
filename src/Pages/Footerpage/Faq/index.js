import React, { useEffect } from 'react'
import Footer from '../../../Container/Footer'

const Faqs = () => {
    useEffect(() => {
        // document.getElementById("particles-js").style.display = "none";
        document.getElementById("navTop").style.background = "#10152c";
        // document.getElementById("particles-js").style.backgroundImage = "transparent";
        window.scrollTo(0, 0)
    }, [])
    return (
        <>
            <div className="mt-73 faqs">
                <div className="headingTop py-5">
                    <div className="container">
                        <div className="heading-inner">
                            <h1>Frequently Asked Questions</h1>
                        </div>
                    </div>
                </div>
                
                <div className="faqs-inner py-5">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8 col-lg-7 col-xl-6">
                                <div className="seaction-heading mb-5">
                                    <h3 className="font-weight-bold">You have questions, we have answers</h3>
                                    <h5>If your question isn’t below, email us at <a href="mailto:support@thebulb.com">support@thebulb.com</a> and a team member will get right back to you!</h5>
                                </div>
                            </div>
                            <div className="col-12">
                                <div className="accordion mb-5" id="accordionExample">
                                    <div className="card">
                                        <div className="card-header" id="headingOne">
                                            <h2 className="mb-0 d-flex justify-content-between" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                Lorem Ipsum dolor amet?
                                                <div className="learn-more d-flex align-items-center">
                                                    <span>Learn more</span>
                                                    <span className="viewless">View less</span>
                                                    <i className="fa fa-plus ml-5"></i>
                                                </div>
                                            </h2>
                                        </div>

                                        <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div className="card-body">
                                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card">
                                        <div className="card-header" id="headingTwo">
                                            <h2 className="mb-0 d-flex justify-content-between collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                                Lorem Ipsum dolor amet?
                                                <div className="learn-more d-flex align-items-center">
                                                    <span>Learn more</span>
                                                    <span className="viewless">View less</span>
                                                    <i className="fa fa-plus ml-5"></i>
                                                </div>
                                            </h2>
                                        </div>
                                        <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                            <div className="card-body">
                                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card">
                                        <div className="card-header" id="headingThree">
                                            <h2 className="mb-0 d-flex justify-content-between collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                                Lorem Ipsum dolor amet?
                                                <div className="learn-more d-flex align-items-center">
                                                    <span>Learn more</span>
                                                    <span className="viewless">View less</span>
                                                    <i className="fa fa-plus ml-5"></i>
                                                </div>
                                            </h2>
                                        </div>
                                        <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                            <div className="card-body">
                                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card">
                                        <div className="card-header" id="headingThree">
                                            <h2 className="mb-0 d-flex justify-content-between collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                                Lorem Ipsum dolor amet?
                                                <div className="learn-more d-flex align-items-center">
                                                    <span>Learn more</span>
                                                    <span className="viewless">View less</span>
                                                    <i className="fa fa-plus ml-5"></i>
                                                </div>
                                            </h2>
                                        </div>
                                        <div id="collapseFour" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                            <div className="card-body">
                                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card">
                                        <div className="card-header" id="headingThree">
                                            <h2 className="mb-0 d-flex justify-content-between collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                                Lorem Ipsum dolor amet?
                                                <div className="learn-more d-flex align-items-center">
                                                    <span>Learn more</span>
                                                    <span className="viewless">View less</span>
                                                    <i className="fa fa-plus ml-5"></i>
                                                </div>
                                            </h2>
                                        </div>
                                        <div id="collapseFive" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                            <div className="card-body">
                                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card">
                                        <div className="card-header" id="headingThree">
                                            <h2 className="mb-0 d-flex justify-content-between collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                                                Lorem Ipsum dolor amet?
                                                <div className="learn-more d-flex align-items-center">
                                                    <span>Learn more</span>
                                                    <span className="viewless">View less</span>
                                                    <i className="fa fa-plus ml-5"></i>
                                                </div>
                                            </h2>
                                        </div>
                                        <div id="collapseSix" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                            <div className="card-body">
                                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer /> 
        </>
    )
}
export default Faqs
