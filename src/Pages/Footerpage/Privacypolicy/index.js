import React, { useEffect} from 'react'
import Footer from '../../../Container/Footer'
const Privacypolicy = () => {
    useEffect(() => {
        // document.getElementById("particles-js").style.display = "none";
        document.getElementById("navTop").style.background = "#10152c";
        // document.getElementById("particles-js").style.backgroundImage = "transparent";
        window.scrollTo(0, 0)
    }, [])
    return (
        <>
            <div className="mt-73 privacy-policy">
                <div className="headingTop py-5">
                    <div className="container">
                        <div className="heading-inner">
                            <h1>The Bulb Privacy Policy</h1>
                        </div>
                    </div>
                </div>
                
                <div className="privacy-inner py-5 mb-4">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-10">
                                <h3 className="section-title">Introduction</h3>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea</p>

                                <p><strong>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,</strong> sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores</p>

                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores</p>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer /> 
        </>
    )
}

export default Privacypolicy
