import React, { useEffect, useState } from 'react'
import Footer from '../../../Container/Footer'
// import {recentpost} from '../../../Const/data'
import Newsfeedcard from '../../../Components/Newsfeedcard'

const Resourses = () => {

    const [filter, setFilter] = useState(false)
    const [value, setValue] = useState('ALL')

    function filterdata(name) {
        setValue(name)
        setFilter(true)
    }

    useEffect(() => {
        document.getElementById("navTop").style.background = "#10152c";
        window.scrollTo(0, 0)
    }, [])


    return (
        <>
            <div className="mt-73 py-5 resources">
                <div className="container">
                    <div className="row mb-4">
                        <div className="col-sm-6">
                            <h3 className="section-heading">Resources</h3>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group text-sm-right">
                                <div className="search position-relative">
                                    <input type="search" placeholder="Search" />
                                    <i className="fas fa-search position-absolute"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="resources-inner">
                        <div className="resources-tab">
                            <div className="resources-filter newsfeed-inner d-inline-block w-100 mb-3">
                                <ul className="pl-0 mb-4 newsfeed-tabs nav">
                                    <li onClick={() => { setFilter(false); setValue('ALL') }} className="nav-item"><span className={`nav-link ${value === 'ALL' && "active"}`}>ALL</span></li>
                                    <li onClick={() => filterdata('Design and UI')} className="nav-item"><span className={`nav-link ${value === 'Design and UI' && "active"}`}>Design and UI</span></li>
                                    <li onClick={() => filterdata('Web Developemt')} className="nav-item"><span className={`nav-link ${value === 'Web Developemt' && "active"}`}>Web Developemt</span></li>
                                    <li onClick={() => filterdata('Programming')} className="nav-item"><span className={`nav-link ${value === 'Programming' && "active"}`}>Programming</span></li>
                                </ul>
                            </div>
                            <div className="row">
                                {/* <Newsfeedcard
                                    list={filter && filter ? recentpost.filter(x => (x.category === value)) : recentpost}
                                    category={true}
                                    content={true}
                                /> */}

                                <div className="col-12 text-center">
                                    <button className="btn btn-outline-warning tex-warning px-5 py-2 my-3">View more</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default Resourses