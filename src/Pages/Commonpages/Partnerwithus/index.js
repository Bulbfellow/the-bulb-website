import React from 'react'
// import whyPartnerImage from '../../../Assets/img/tech_incubation.webp'
// import benefitsI from '../../../Assets/img/benefits-partner1.png'
// import benefitsII from '../../../Assets/img/benefits-partner2.png'
// import benefitsIII from '../../../Assets/img/benefits-partner3.png'
// import PartnerLogoI from '../../../Assets/img/paystack.png'
// import PartnerLogoII from '../../../Assets/img/andela.png'
// import PartnerLogoIII from '../../../Assets/img/speedie.png'
// import PartnerLogoIV from '../../../Assets/img/genrosys.png'
import Footer from '../../../Container/Footer'
import AnchorLink from 'react-anchor-link-smooth-scroll'
import { Partnership } from '../../../common/function'
import AlertDialog from '../../../Components/Madal'
import { State, City, Uploadfile } from '../../../Action/commonAction'
import CircularProgress from '@material-ui/core/CircularProgress'
import Inputcomponents from '../../../Components/Inputarea'
import Loader from '../../../Components/Loader'
import { Fileupload } from '../../../common/commonfunction'
let errormsg = "This field is required."

class Partnerwithus extends React.Component {
    constructor() {
        super()
        this.state = {
            fields: {},
            errors: {},
            selectedFile: null,
            imageurl: '',
            fileuploadloader: false,
            fileuploadloadermsg: '',
            loader: false,
            status: null,
            setOpen: false,
            message: "",
            file: null,
            uploadstatus: null
        }
    }

    componentDidMount() {
        document.getElementById("navTop").style.background = "#10152c";
        window.addEventListener('scroll', this.handleScroll, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    HandleChange = e => {
        let fields = this.state.fields;
        fields[e.target.name] = e.target.value;
        this.setState({ fields });
    }

    handleClose = () => {
        this.setState({
            setOpen: false
        })
    };

    handleUploadFile = async (e) => {
        this.setState({
            fileuploadloader: true,
            fileuploadloadermsg: ""
        })
        let filesize = e.target.files[0].size
        console.log("filesize", filesize)
        if (1024000 >= filesize) {
            await Fileupload(e.target.files[0])
                .then(response => {
                    this.setState({
                        imageurl: response.data.image_url,
                        fileuploadloader: false,
                        uploadstatus: response.status
                    })
                })
                .catch(error => {
                    console.log("error", error)
                    this.setState({
                        fileuploadloadermsg: error,
                        fileuploadloader: false
                    })
                })
        }
    }

    clearState() {
        console.log("hit")
        let fields = {};
        fields['Fname'] = '';
        fields['Lname'] = '';
        fields['Company'] = '';
        fields['Notes'] = '';
        fields['Phoneno'] = '';
        this.setState({
            fields: fields,
            file: '',
            uploadstatus: null
        })
    }

    Submitform = async (e) => {
        e.preventDefault();
        if (this.validateForm()) {
            this.setState({
                loader: true,
                uploadstatus: null
            })
            let data = {
                first_name: this.state.fields.Fname,
                last_name: this.state.fields.Lname,
                company_name: this.state.fields.Company,
                mobile: this.state.fields.Phoneno,
                notes: this.state.fields.Notes,
                type: "NGO",
                file_url: this.state.imageurl
            }
            await Partnership(data)
                .then(response => {
                    this.setState({
                        message: response.message,
                        loader: false,
                        setOpen: true,
                        status: response.status
                    })
                }).catch(err => {
                    console.log("error for the submit form", err)
                    this.setState({
                        loader: false,
                        message: err,
                        status: 0,
                        setOpen: true,
                    })
                })
            this.clearState()
        }
    }


    validateForm() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (!fields['Fname']) {
            formIsValid = false;
            errors["Fname"] = errormsg
        }

        if (typeof fields['Fname'] !== "undefined") {
            if (!fields['Fname'].match(/^[a-zA-Z]*$/)) {
                formIsValid = false;
                errors['Fname'] = errormsg
            }
        }
        if (!fields['Lname']) {
            formIsValid = false;
            errors["Lname"] = errormsg
        }
        if (typeof fields['Lname'] !== 'undefined') {
            if (!fields['Lname'].match(/^[a-zA-Z]*$/)) {
                formIsValid = false;
                errors['Lname'] = errormsg
            }
        }
        if (!fields['Phoneno']) {
            formIsValid = false;

            errors["Phoneno"] = errormsg
        }
        if (typeof fields['Phoneno'] !== 'undefined') {
            if (!fields['Phoneno'].match(/^[0-9+]/)) {
                formIsValid = false;
                errors['Phoneno'] = errormsg
            }
        }
        if (!fields['Company']) {
            formIsValid = false;
            errors["Company"] = errormsg
        }

        if (!fields['Notes']) {
            formIsValid = false;
            errors["Notes"] = errormsg
        }

        if (this.state.imageurl == null) {
            formIsValid = false;
            errors["file"] = errormsg
        }
        if (this.state.imageurl == '') {
            formIsValid = false;
            errors["file"] = errormsg
        }
        this.setState({ errors: errors });
        return formIsValid;
    }

    nav = React.createRef();

    handleScroll = () => {
        var lastScrollY = window.scrollY;
        if (lastScrollY > 875) {
            this.setState({
                currenttab: 'Benefitsofpartnership'
            })
        }
        if (lastScrollY > 1800) {
            this.setState({
                currenttab: 'Ourpartnershipstartshere'
            })
        }
        if (lastScrollY < 875) {
            this.setState({
                currenttab: 'WhyPartnerwithus'
            })
        }
    };

    render() {
        const { formErrors } = this.state;
        console.log("loader", this.state.loader)
        console.log("PartnershipState", this.props.PartnershipState)
        return (
            <>
                <div className="partners-withus mt-73" ref={this.nav}>
                    {/* <div className="fixed-tab position-fixed">
                        <div onClick={() => this.setCurrenttab('WhyPartnerwithus')} className={`fix-tab ${this.state.currenttab === 'WhyPartnerwithus' && "active-tab"}`} >
                            <AnchorLink offset='80' href='#whyPartner'></AnchorLink>
                            <span className="tabtitle">Why Partner with us</span>
                        </div>
                        <div onClick={() => this.setCurrenttab('Benefitsofpartnership')} className={`fix-tab ${this.state.currenttab === 'Benefitsofpartnership' && "active-tab"}`}>
                            <AnchorLink offset='80' href='#benefitsPartnership'></AnchorLink>
                            <span className="tabtitle">Benefits of partnership</span>
                        </div>
                        <div onClick={() => this.setCurrenttab('Ourpartnershipstartshere')} className={`fix-tab ${this.state.currenttab === 'Ourpartnershipstartshere' && "active-tab"}`}>
                            <AnchorLink offset='77' href='#partnershipHere'></AnchorLink>
                            <span className="tabtitle">Our partnership starts here</span>
                        </div>
                    </div> */}
                    <div className="bg-partners parallax py-5">
                        <div className="container py-md-5 my-md-4">
                            <h1 className="text-white font-weight-bold">Be a catalyst for innovation</h1>
                            <p className="py-1 text-white">Our partners are visionary organizations who are <br /> focused on transformative technology.</p>
                            <AnchorLink offset='77' href='#partnershipHere'><button className="btn text-white btn-warning px-5">Get Started</button></AnchorLink>
                        </div>
                    </div>

                    <div className="why-partners py-5" id="whyPartner">
                        <div className="container pt-md-4">
                            <div className="row flex-column-reverse flex-md-row align-items-center">
                                <div className="col-md-6">
                                    <h3 className="section-heading mb-4">Making a difference together</h3>
                                    <p className="pr-md-5">Corporate and community partnerships help us positively influence innovation in Africa. The Bulb works with a diverse group of local and international partners who share our vision for the success of tech innovation.</p>
                                </div>
                                <div className="col-md-6">
                                    <div className="whypartner-img">
                                        <img src='https://thebulb.africa/Images/individual/tech_incubation.jpg' alt="Why Partner with us" className="img-fluid ml-auto d-table" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* 
                    <div className="benefts-partnership py-5" id="benefitsPartnership">
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    <h3 className="section-heading mb-5">Benefits of partnership</h3>
                                </div>
                                <div className="col-md-4">
                                    <div className="benefits-inner">
                                        <div className="benefits-img mb-2">
                                            <img src={benefitsI} alt="Benefits Partnership" className="img-fluid" />
                                        </div>
                                        <div className="benefits-content">
                                            <h4>Lorem ipsum dolor amet</h4>
                                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="benefits-inner">
                                        <div className="benefits-img mb-2">
                                            <img src={benefitsII} alt="Benefits Partnership" className="img-fluid" />
                                        </div>
                                        <div className="benefits-content">
                                            <h4>Lorem ipsum dolor amet</h4>
                                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="benefits-inner">
                                        <div className="benefits-img mb-2">
                                            <img src={benefitsIII} alt="Benefits Partnership" className="img-fluid" />
                                        </div>
                                        <div className="benefits-content">
                                            <h4>Lorem ipsum dolor amet</h4>
                                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> */}

                    {/* <div className="our-partners pt-2 pb-5">
                        <div className="container pb-md-4">
                            <div className="row">
                                <div className="col-12">
                                    <h3 className="section-heading mb-5">Our partners</h3>
                                </div>
                                <div className="col-md-3 col-sm-6">
                                    <div className="partner-logo text-center">
                                        <img src={PartnerLogoI} alt="Our Partners Logo" className="img-fluid" />
                                    </div>
                                </div>
                                <div className="col-md-3 col-sm-6">
                                    <div className="partner-logo text-center">
                                        <img src={PartnerLogoII} alt="Our Partners Logo" className="img-fluid" />
                                    </div>
                                </div>
                                <div className="col-md-3 col-sm-6">
                                    <div className="partner-logo text-center">
                                        <img src={PartnerLogoIII} alt="Our Partners Logo" className="img-fluid" />
                                    </div>
                                </div>
                                <div className="col-md-3 col-sm-6">
                                    <div className="partner-logo text-center">
                                        <img src={PartnerLogoIV} alt="Our Partners Logo" className="img-fluid" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> */}

                    <div className="bg-partnership parallax partnership-here py-5" id="partnershipHere">
                        <div className="container">
                            <div className="row py-md-4 justify-content-center">
                                <div className="col-md-10">
                                    <form onSubmit={this.Submitform}>
                                        <div className="partnership-form">
                                            <h3 className="section-heading text-center mb-4 pb-2">Our partnership starts here</h3>
                                            <div className="row">
                                                <Inputcomponents
                                                    divclassname={"form-group col-md-6"}
                                                    label={"First name"}
                                                    Errorname={this.state.errors.Fname}
                                                    errormsg={errormsg}
                                                    type={"text"}
                                                    HandleChange={this.HandleChange}
                                                    name={"Fname"}
                                                    value={this.state.fields.Fname}
                                                />

                                                <Inputcomponents
                                                    divclassname={"form-group col-md-6"}
                                                    label={"Last name"}
                                                    Errorname={this.state.errors.Lname}
                                                    errormsg={errormsg}
                                                    type={"text"}
                                                    HandleChange={this.HandleChange}
                                                    name={"Lname"}
                                                    value={this.state.fields.Lname}
                                                />

                                            </div>
                                            <div className="row">
                                                <Inputcomponents
                                                    divclassname={"form-group col-md-6"}
                                                    label={"Company name"}
                                                    Errorname={this.state.errors.Company}
                                                    errormsg={errormsg}
                                                    type={"text"}
                                                    HandleChange={this.HandleChange}
                                                    name={"Company"}
                                                    value={this.state.fields.Company}
                                                />

                                                <Inputcomponents
                                                    divclassname={"form-group col-md-6"}
                                                    label={"Phone number"}
                                                    Errorname={this.state.errors.Phoneno}
                                                    errormsg={errormsg}
                                                    type={"number"}
                                                    HandleChange={this.HandleChange}
                                                    name={"Phoneno"}
                                                    value={this.state.fields.Phoneno}
                                                />

                                            </div>

                                            <div className="row">
                                                <div className="form-group col-md-12">
                                                    <label for="Notes">Notes {this.state.errors.Notes === errormsg ? <span className="errorMsg">*</span> : <span>*</span>}</label>
                                                    <textarea
                                                        rows="3"
                                                        className="form-control"
                                                        id="Notes"
                                                        onChange={this.HandleChange}
                                                        value={this.state.fields.Notes}
                                                        name="Notes"
                                                    />
                                                    <div className="errorMsg">{this.state.errors.Notes}</div>
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="form-group col-md-12">
                                                    <label htmlFor="AttachPresentaion">Attach a presentation (Pdf format)

{this.state.errors.file === errormsg ? <span className="errorMsg">*</span> : <span>*</span>}</label>
                                                    <input
                                                        type="file"
                                                        className="d-block py-2"
                                                        id="AttachPresentaion"
                                                        name='file'
                                                        onChange={this.handleUploadFile}
                                                        value={this.state.file}
                                                        accept=".pdf"
                                                    />
                                                    {this.state.fileuploadloader && this.state.fileuploadloader && <CircularProgress />}
                                                    {this.state.fileuploadloadermsg}
                                                    <div className="errorMsg">
                                                        {this.state.errors.file}
                                                    </div>
                                                    <div className="succesMsg">
                                                        {this.state.uploadstatus === "1" && "File upload succesfully"}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="form-group text-center col-md-12">
                                                    <button type="submit" value="ngo" className="btn btn-warning text-white px-5 py-2 mt-3">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    {this.state.loader && this.state.loader &&
                                        <Loader />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <AlertDialog
                    open={this.state.setOpen}
                    close={this.handleClose}
                    status={this.state.status}
                    message={this.state.message}
                />
                <Footer />
            </>
        )
    }
}
export default Partnerwithus
