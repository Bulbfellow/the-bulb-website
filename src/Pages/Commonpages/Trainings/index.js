import React, { useEffect, useState } from 'react'
import Footer from '../../../Container/Footer'
import Corporate from '../../../Container/Traning/Corporate'
import Individuals from '../../../Container/Traning/Individuals'

const Traning = ({ changetype }) => {
    const [traningtype, setTraningtype] = useState(changetype === true ? 'Corporate' : 'Individuals')
    useEffect(() => {
        document.getElementById("navTop").style.background = "#10152c";
    }, [])
    return (
        <>
            <div className="training mt-73">
                <div className="training-banner parallax py-5">
                    <div className="container">
                        <div className="row py-md-5 text-center">
                            <div className="col-md-12">
                                <h1 className="text-white font-weight-bold">Building Tomorrow's Leaders</h1>
                                <p className="py-3 mx-auto text-white">Our training model is reflective of our mission to create experiential learning opportunities for incubators, students and businesses with transferable and transportable skill sets for tomorrow’s leaders.
</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="ourcourse py-5">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="heading mb-4">
                                    <h3 className="font-weight-bold">Training</h3>
                                </div>
                            </div>
                            <div className="col-12">
                                <ul className="nav nav-tabs mb-4 pb-2 border-0" style={{ cursor: 'pointer' }}>
                                    <li className="nav-item" onClick={() => setTraningtype('Individuals')} >
                                        <a className={`nav-link ${traningtype === 'Individuals' && "active"}`}>Individuals</a>
                                    </li>
                                    <li className="nav-item" onClick={() => setTraningtype('Corporate')}>
                                        <a className={`nav-link ${traningtype === 'Corporate' && "active"}`}>Corporate</a>
                                    </li>
                                </ul>

                                <div className="tab-content">
                                    <div className="tab-pane fade show active">
                                        <div className="row">
                                            {traningtype === "Individuals" && <Individuals />}
                                            {traningtype === "Corporate" && <Corporate />}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default Traning