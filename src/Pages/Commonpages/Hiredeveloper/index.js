import React, { Component } from 'react'
import Footer from '../../../Container/Footer'
import AnchorLink from 'react-anchor-link-smooth-scroll'
import AlertDialog from '../../../Components/Madal'
import Inputcomponents from '../../../Components/Inputarea'
import Loader from '../../../Components/Loader'
import { Hiredeveloper } from '../../../common/function'

let errormsg = "This field is required."

class Hireadeveloper extends Component {
    constructor() {
        super();
        this.state = {
            fields: {
                Fname: '',
                Lname: '',
                Email: '',
                Country: '',
                State: '',
                Phoneno: '',
                CompanyName: ''
            },
            errors: {},
            loader: false,
            status: null,
            setOpen: false,
            message: ""
        }
    }
    componentDidMount() {
        document.getElementById("navTop").style.background = "#10152c";
    }


    handleChange = (e) => {
        let fields = this.state.fields;
        fields[e.target.name] = e.target.value;
        this.setState({ fields })
    }
    handleClose = () => {
        this.setState({
            setOpen: false
        })
    };

    validateFrom() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;


        if (!fields['Fname']) {
            formIsValid = false;
            errors["Fname"] = errormsg
        }
        if (typeof fields['Fname'] !== "undefined") {
            if (!fields['Fname'].match(/^[a-zA-Z]/)) {
                errors["Fname"] = errormsg
            }
        }

        if (!fields['Lname']) {
            formIsValid = false;
            errors["Lname"] = errormsg
        }
        if (typeof fields['Lname'] !== "undefined") {
            if (!fields['Lname'].match(/^[a-zA-Z]/)) {
                errors["Lname"] = errormsg
            }
        }
        if (!fields['Email']) {
            formIsValid = false;
            errors["Email"] = errormsg
        }
        if (typeof fields['Email'] !== "undefined") {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i)
            if (!pattern.test(fields['Email'])) {
                formIsValid = false;
                errors["Email"] = errormsg
            }
        }
        if (!fields['Phoneno']) {
            formIsValid = false;
            errors["Phoneno"] = errormsg
        }
        if (typeof fields['Phoneno'] !== 'undefined') {
            if (!fields['Phoneno'].match(/^[0-9+]/)) {
                formIsValid = false;
                errors['Phoneno'] = errormsg
            }
        }
        if (!fields['State']) {
            formIsValid = false;
            errors["State"] = errormsg
        }
        if (typeof fields['State'] !== "undefined") {
            if (!fields['State'].match(/^[a-zA-Z]/)) {
                errors["State"] = errormsg
            }
        }
        if (!fields['CompanyName']) {
            formIsValid = false;
            errors["CompanyName"] = errormsg
        }

        if (!fields['Country']) {
            formIsValid = false;
            errors["Country"] = errormsg
        }
        if (typeof fields['Country'] !== "undefined") {
            if (!fields['Country'].match(/^[a-zA-Z]/)) {
                errors["Country"] = errormsg
            }
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    clearState() {
        this.state = {
            fields: {
                Fname: '',
                Lname: '',
                Email: '',
                Country: '',
                State: '',
                Phoneno: '',
                CompanyName: ''
            },
            errors: {},
        }
    }

    Submitform = async (e) => {
        e.preventDefault();
        if (this.validateFrom()) {
            let fields = {};
            fields['Fname'] = '';
            fields['Lname'] = '';
            fields['Email'] = '';
            fields['Country'] = '';
            fields['State'] = '';
            fields['Phoneno'] = '';
            fields['CompanyName'] = ''
            this.setState({ fields: fields, loader: true })
            let data = {
                first_name: this.state.fields.Fname,
                last_name: this.state.fields.Lname,
                company_name: this.state.fields.CompanyName,
                work_email: this.state.fields.Email,
                country: this.state.fields.Country,
                state: this.state.fields.State,
                mobile: this.state.fields.Phoneno
            }
            await Hiredeveloper(data)
            .then(response => {
                this.setState({
                    message: response.message,
                    loader: false,
                    setOpen: true,
                    status: response.status
                })
            }).catch(err => {
                this.setState({
                    loader: false,
                    message: err,
                    status: 0,
                    setOpen: true,
                })
            })
            this.clearState()
        }
    }

    render() {
        return (
            <>
                <div className="hiredeveloper mt-73">
                    <div className="become-developer-slide parallax py-5">
                        <div className="container">
                            <div className="row text-center justify-content-center">
                                <div className="col-md-8 py-md-4">
                                    <h1 className="text-white font-weight-bold">Dream big, build smart.</h1>
                                    <p className="py-3 text-white">The Bulbs developers are trained, tested and trusted to deliver - No Story. Our developers can help you start a project from scratch or scale up an existing IT software or infrastructure</p>
                                    <AnchorLink offset='80' href='#hiredeveloper'><button className="btn text-white btn-warning px-5">Hire a developer</button></AnchorLink>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="whychoose py-5">
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    <h2 className="section-heading text-center mb-3 mb-md-5">Why choose us</h2>
                                </div>
                                <div className="col-sm-4">
                                    <div className="whychoose-inner">
                                        <h4>Trained</h4>
                                        <p>Our developers are IT Gurus. Hired for character and trained for skill. We’ll equip you with a developer trained in popular field such as iOS, Android, Javascript, React, Angular, Node, PHP, Python, Ruby and Java</p>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="whychoose-inner">
                                        <h4>Tested</h4>
                                        <p>Every one of our developers is Bulb certified. World-class standards in vocational aptitude keep our developers at the top of their game, while our in-house training keeps them ahead of the curve.</p>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="whychoose-inner">
                                        <h4>Trusted</h4>
                                        <p>We’re careful not only to treat your product with care but also your company culture. Through personality assessments, we assign you the best fitting developer that blends with your company culture and values.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="hiredev py-5" id="hiredeveloper">
                        <div className="container">
                            <div className="row justify-content-center">
                                <div className="col-12">
                                    <h2 className="section-heading text-center mb-3 mb-md-5">Hire a developer today</h2>
                                </div>
                                <div className="col-md-8">
                                    <form onSubmit={this.Submitform}>
                                        <div className="hiredev-form">
                                            <div className="row">
                                                <Inputcomponents
                                                    divclassname={"col-md-6"}
                                                    label={"First name"}
                                                    Errorname={this.state.errors.Fname}
                                                    errormsg={errormsg}
                                                    type={"text"}
                                                    HandleChange={this.handleChange}
                                                    name={"Fname"}
                                                    value={this.state.fields.Fname}
                                                />

                                                <Inputcomponents
                                                    divclassname={"col-md-6"}
                                                    label={"Last name"}
                                                    Errorname={this.state.errors.Lname}
                                                    errormsg={errormsg}
                                                    type={"text"}
                                                    HandleChange={this.handleChange}
                                                    name={"Lname"}
                                                    value={this.state.fields.Lname}
                                                />
                                            </div>

                                            <div className="row">
                                                <Inputcomponents
                                                    divclassname={"col-md-6"}
                                                    label={"Company name"}
                                                    Errorname={this.state.errors.CompanyName}
                                                    errormsg={errormsg}
                                                    type={"text"}
                                                    HandleChange={this.handleChange}
                                                    name={"CompanyName"}
                                                    value={this.state.fields.CompanyName}
                                                />
                                                <Inputcomponents
                                                    divclassname={"col-md-6"}
                                                    label={"Work email"}
                                                    Errorname={this.state.errors.Email}
                                                    errormsg={errormsg}
                                                    type={"text"}
                                                    HandleChange={this.handleChange}
                                                    name={"Email"}
                                                    value={this.state.fields.Email}
                                                />
                                            </div>

                                            <div className="row">
                                                <Inputcomponents
                                                    divclassname={"col-md-6"}
                                                    label={"Country"}
                                                    Errorname={this.state.errors.Country}
                                                    errormsg={errormsg}
                                                    type={"text"}
                                                    HandleChange={this.handleChange}
                                                    name={"Country"}
                                                    value={this.state.fields.Country}
                                                />
                                                <Inputcomponents
                                                    divclassname={"col-md-6"}
                                                    label={"State"}
                                                    Errorname={this.state.errors.State}
                                                    errormsg={errormsg}
                                                    type={"text"}
                                                    HandleChange={this.handleChange}
                                                    name={"State"}
                                                    value={this.state.fields.State}
                                                />
                                            </div>

                                            <div className="row">
                                                <Inputcomponents
                                                    divclassname={"col-md-12"}
                                                    label={"Phone number"}
                                                    Errorname={this.state.errors.Phoneno}
                                                    errormsg={errormsg}
                                                    type={"text"}
                                                    HandleChange={this.handleChange}
                                                    name={"Phoneno"}
                                                    value={this.state.fields.Phoneno}
                                                />
                                            </div>

                                            <div className="row">
                                                <div className="col-md-12">
                                                    <div className="form-group text-center">
                                                        <button
                                                            className="btn btn-warning px-5 py-2 my-3 text-white">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    {this.state.loader && this.state.loader &&
                                        <Loader />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
                <AlertDialog
                    open={this.state.setOpen}
                    close={this.handleClose}
                    status={this.state.status}
                    message={this.state.message}
                />
            </>
        )
    }
}

export default Hireadeveloper
