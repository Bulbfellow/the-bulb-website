import React, { useEffect} from 'react'
import Footer from '../../../Container/Footer'

const Community = () => {
    useEffect(() => {
        // document.getElementById("particles-js").style.display = "none";
        document.getElementById("navTop").style.background = "#10152c";
        // document.getElementById("particles-js").style.backgroundImage = "transparent";
    }, [])
    return (
        <>
            <div className="startup parallax mt-73 py-5">
                <div className="startup-inner py-md-5">
                    <div className="container py-md-5 px-md-5 my-md-5 text-center text-white">
                        <h1>Lorem ipsum dolor sit amet, consetetur sadips</h1>
                        <p className="py-3">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor I nvidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores</p>
                        <div className="startup-btn flex-wrap d-flex justify-content-center align-items-center">
                            <a target="_blank" href="https://slack.com/intl/en-in/"><button className="btn btn-cus-warning px-5 mr-sm-5">Join the community</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default Community