import React, { useEffect } from 'react'
import Footer from '../../../Container/Footer'
import { Link } from 'react-router-dom'

const Startups = () => {
	useEffect(() => {
		document.getElementById("navTop").style.background = "#10152c";
	}, [])
	return (
		<>
			<div className="startup parallax mt-73 py-5">
				<div className="startup-inner py-md-5">
					<div className="container py-md-5 px-md-5 my-md-5 text-center text-white">
						<h1>Startup to scaleup</h1>
						<p className="py-3">Helping African entrepreneurs grow tech ideas into business realities.
						<br/>We find, nurture and grow startups through our incubation and accelerator programs. </p>
						<div className="startup-btn flex-wrap d-flex justify-content-center align-items-center">
							<Link to="/submit-an-idea"><button className="btn btn-cus-warning px-5 mr-sm-5">Submit an idea</button></Link>
							<Link to="/apply-incubator"><button className="btn btn-cus-outline-warning px-5">Apply for incubator</button></Link>
						</div>
					</div>
				</div>
			</div>
			<Footer />
		</>
	)
}
export default Startups