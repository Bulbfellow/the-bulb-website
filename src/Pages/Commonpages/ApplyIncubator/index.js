import React, { Component } from 'react'
import Footer from '../../../Container/Footer'
import AlertDialog from '../../../Components/Madal'
import CircularProgress from '@material-ui/core/CircularProgress'
import { Applyincubator } from '../../../common/function'
import { Fileupload, Statelist, Citylist } from '../../../common/commonfunction'
import Inputcomponents from '../../../Components/Inputarea'
import Loader from '../../../Components/Loader'

let errormsg = "This field is required."
class ApplyIncubator extends Component {
	constructor() {
		super();
		this.state = {
			fields: {
				title: '',
				fname: '',
				lname: '',
				phoneno: '',
				email: '',
				aboutproductYourself: '',
				aboutYourself: ''
			},
			errors: {},
			isChecked: false,
			State: '',
			uploadstatus: null,
			City: '',
			cityloader: false,
			errordropdownmsg: '',
			imageurl: '',
			fileuploadloader: false,
			fileuploadloadermsg: '',
			status: null,
			setOpen: false,
			message: "",
			statearray: [],
			cityarray: [],
		}
	}

	async componentDidMount() {
		document.getElementById("navTop").style.background = "#10152c";
		let statelist = await Statelist()
		this.setState({
			statearray: statelist.list
		})
	}


	clearState() {
		console.log("clear State")
		let fields = {
			title: '',
			fname: '',
			lname: '',
			phoneno: '',
			email: '',
			aboutproductYourself: '',
			aboutYourself: ''
		};
		this.setState({
			fields: fields,
			City: '',
			isChecked: false,
			imageurl: null,
			State: '',
			file: '',
			State: '',
			loader: false,
			fileuploadloader: false,
			uploadstatus: null,
		});
	}

	toggleChange = () => {
		this.setState({
			isChecked: !this.state.isChecked
		})
	}

	hanndleChange = (e) => {
		let fields = this.state.fields;
		fields[e.target.name] = e.target.value;
		this.setState({ fields })
	}

	handleStateDropdown = async (e) => {
		var strng = e.target.value;
		var arr = strng.split(',');
		this.setState({ State: arr[1], cityloader: true })
		let citylist = await Citylist(arr[0])
		this.setState({
			cityloader: false,
			cityarray: citylist.list
		})
	}


	handleCityDropdown = (e) => {
		this.setState({
			City: e.target.value
		})
	}

	handleUploadFile = async (e) => {
		this.setState({
			fileuploadloader: true,
			fileuploadloadermsg: ""
		})
		let filesize = e.target.files[0].size
		if (5242880 >= filesize) {
			await Fileupload(e.target.files[0])
				.then(response => {
					console.log("response", response)
					this.setState({
						imageurl: response.data.image_url,
						fileuploadloader: false,
						uploadstatus: response.status
					})
				})
				.catch(error => {
					console.log("error", error)
					this.setState({
						fileuploadloadermsg: error,
						fileuploadloader: false
					})
				})
		}
		else {
			this.setState({
				fileuploadloadermsg: "please select the file than less than 5 mb",
				fileuploadloader: false
			})
		}
	}

	handleSubmit = async (e) => {
		e.preventDefault();
		console.log("the", this.state.Stateid)
		if (this.validateForm()) {
			this.setState({
				loader: true,
				uploadstatus: null,
			})
			let data = {
				title: this.state.fields.title,
				first_name: this.state.fields.fname,
				last_name: this.state.fields.lname,
				mobile: this.state.fields.phoneno,
				email: this.state.fields.email,
				city: this.state.City,
				state: this.state.State,
				about_self: this.state.fields.aboutYourself,
				about_product: this.state.fields.aboutproductYourself,
				file_url: this.state.imageurl
			}
			await Applyincubator(data)
				.then(response => {
					console.log("response", response)
					this.setState({
						message: response.message,
						loader: false,
						setOpen: true,
						status: response.status
					})
				}).catch(err => {
					console.log("error for the submit form", err)
					this.setState({
						loader: false,
						message: err,
						status: 0,
						setOpen: true,
					})
				})
			this.clearState()
		}
	}

	validateForm() {
		let fields = this.state.fields;
		let errors = {};
		let formIsValid = true;
		if (!fields['title']) {
			formIsValid = false;
			errors["title"] = errormsg
		}
		if (typeof fields['title'] !== "undefined") {
			if (!fields['title'].match(/^[a-zA-Z. ]/)) {
				errors["title"] = errormsg
			}
		}
		if (!fields['fname']) {
			formIsValid = false;
			errors["fname"] = errormsg
		}
		if (typeof fields['fname'] !== "undefined") {
			if (!fields['fname'].match(/^[a-zA-Z]/)) {
				errors["fname"] = errormsg
			}
		}
		if (!fields['lname']) {
			formIsValid = false;
			errors["lname"] = errormsg
		}
		if (typeof fields['lname'] !== "undefined") {
			if (!fields['lname'].match(/^[a-zA-Z]/)) {
				errors["lname"] = errormsg
			}
		}
		if (!fields['email']) {
			formIsValid = false;
			errors["email"] = errormsg
		}
		if (typeof fields['email'] !== "undefined") {
			var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i)
			if (!pattern.test(fields['email'])) {
				formIsValid = false;
				errors["email"] = errormsg
			}
		}
		if (!fields['phoneno']) {
			formIsValid = false;
			errors["phoneno"] = errormsg
		}
		if (typeof fields['phoneno'] !== 'undefined') {
			if (!fields['phoneno'].match(/^[0-9+]/)) {
				formIsValid = false;
				errors['phoneno'] = errormsg
			}
		}
		if (!fields['aboutYourself']) {
			formIsValid = false;
			errors["aboutYourself"] = errormsg
		}
		if (!fields['aboutproductYourself']) {
			formIsValid = false;
			errors["aboutproductYourself"] = errormsg
		}

		if (this.state.isChecked == false) {
			formIsValid = false;
			errors["check"] = "*Please Read and accept term and Conditions"
		}

		if (this.state.imageurl == null) {
			formIsValid = false;
			errors["file"] = errormsg
		}
		if (this.state.imageurl == '') {
			formIsValid = false;
			errors["file"] = errormsg
		}
		this.setState({ errors: errors });
		return formIsValid;
	}

	handleClose = () => {
		this.setState({
			setOpen: false,
			loader: false,
			fileuploadloader: false,
			isChecked: false
		})
	};

	render() {
		return (
			<>
				<div className="mt-73 submit-idea">
					<div className="submit-idea-title headingTop py-5">
						<div className="container">
							<div className="heading-inner">
								<h1>Apply for incubation</h1>
							</div>
						</div>
					</div>
					<div className="reqest-tour submit-ideaForm mt-md-3 py-3 py-sm-5 mb-5">
						<div className="container">
							<div className="card">
								<div className="card-body">
									<form onSubmit={this.handleSubmit}>
										<div className="request-heading">
											<h3>Individual Information</h3>
											<p className="d-none d-md-block">Please provide us with your information</p>
										</div>
										<div className="row">
											<Inputcomponents
												divclassname={"col-md-4"}
												label={"Title"}
												Errorname={this.state.errors.title}
												errormsg={errormsg}
												type={"text"}
												HandleChange={this.hanndleChange}
												name={"title"}
												value={this.state.fields.title}
											/>
											<Inputcomponents
												divclassname={"col-md-4"}
												label={"First Name"}
												Errorname={this.state.errors.fname}
												errormsg={errormsg}
												type={"text"}
												HandleChange={this.hanndleChange}
												name={"fname"}
												value={this.state.fields.fname}
											/>

											<Inputcomponents
												divclassname={"col-md-4"}
												label={"Last Name"}
												Errorname={this.state.errors.lname}
												errormsg={errormsg}
												type={"text"}
												HandleChange={this.hanndleChange}
												name={"lname"}
												value={this.state.fields.lname}
											/>
										</div>

										<div className="row">
											<Inputcomponents
												divclassname={"col-md-6"}
												label={"Phone Number"}
												Errorname={this.state.errors.phoneno}
												errormsg={errormsg}
												type={"text"}
												HandleChange={this.hanndleChange}
												name={"phoneno"}
												value={this.state.fields.phoneno}
											/>
											<Inputcomponents
												divclassname={"col-md-6"}
												label={"Email Address"}
												Errorname={this.state.errors.email}
												errormsg={errormsg}
												type={"text"}
												HandleChange={this.hanndleChange}
												name={"email"}
												value={this.state.fields.email}
											/>
										</div>

										<div className="row">
											<div className="col-md-6">
												<div className="form-group">
													<label htmlFor="state">State</label>
													<select name="state" id="state" className="form-control" onChange={this.handleStateDropdown}>
														<option defaultValue="" >-select-</option>
														{this.state.statearray.map(state => {
															return (
																<option key={state.id} value={[state.id, state.name]}> {state.name} </option>)
														})}
													</select>
												</div>
											</div>

											<div className="col-md-6">
												<div className="form-group">
													<label htmlFor="City">City</label>
													<select name="city" id="City" className="form-control" onChange={this.handleCityDropdown} value={this.state.City}>
														<option defaultValue="" >-select-</option>

														{this.state.cityarray.map(city => {
															return (
																<option key={city.id} value={city.name}>{city.name}</option>
															)
														})}
													</select>
													{this.state.cityloader && this.state.cityloader && <p>Loading......</p>}
													{this.state.errordropdownmsg}
												</div>
											</div>

											<div className="col-md-12">
												<div className="form-group">
													<label htmlFor="aboutYourself">Tell us about yourself (2000 characters max) {this.state.errors.aboutYourself == errormsg ? <span className="errorMsg">*</span> : <span>*</span>}</label>
													<textarea className="form-control" maxLength="2000" rows="7" id="aboutYourself"
														name="aboutYourself" onChange={this.hanndleChange} value={this.state.fields.aboutYourself}
													></textarea>
													<div className="errorMsg">
														{this.state.errors.aboutYourself}
													</div>
												</div>
											</div>

										</div>
										<div className="request-heading">
											<h3>Your product/Idea</h3>
										</div>

										<div className="row">
											<div className="col-md-12">
												<div className="form-group">
													<label htmlFor="aboutproductYourself">Tell us about your product/Idea (2000 characters max) {this.state.errors.aboutproductYourself == errormsg ? <span className="errorMsg">*</span> : <span>*</span>}</label>
													<textarea className="form-control" maxLength="2000" rows="7" id="aboutproductYourself"
														name="aboutproductYourself" onChange={this.hanndleChange} value={this.state.fields.aboutproductYourself}
													></textarea>
													<div className="errorMsg">
														{this.state.errors.aboutproductYourself}
													</div>
												</div>
											</div>
											<div className="col-md-12 mt-2">
												<div className="form-group">
													<label htmlFor="AttachPresentaion">Attach a presentation (Pdf format)

{this.state.errors.file == errormsg ? <span className="errorMsg">*</span> : <span>*</span>}</label>

													<input
														type="file"
														className="d-block py-2"
														id="AttachPresentaion"
														name='file'
														onChange={this.handleUploadFile}
														filename={this.state.imageurl}
														value={this.state.file}
														accept=".pdf"
													/>
													{this.state.fileuploadloader && this.state.fileuploadloader && <CircularProgress />}
													{this.state.fileuploadloadermsg}
													<small className="d-block pt-2">Maximum upload size: 5 MB</small>
													<div className="errorMsg">
														{this.state.errors.file}
													</div>
													<div className="succesMsg">
														{this.state.uploadstatus === "1" && "File upload succesfully"}
													</div>
												</div>
											</div>
										</div>

										<div className="request-heading">

											<h3>Attestation</h3>
										</div>
										<div className="row">
											<div className="col-md-12 mb-2">
												<div className="form-group">
													<label htmlFor="termsCond" className="checkbox-c">
														<input type="checkbox" name="check" className="mr-3"
															checked={this.state.isChecked} onChange={this.toggleChange} />
I/We hereby confirm that I/We have read the <a href="/terms-and-condition" target="_blank">
															Terms and Conditions</a> of The Hub and agree to same</label>
													<div className="errorMsg">
														{this.state.errors.check}
													</div>
												</div>
											</div>
											<div className="col-md-12">
												<div className="form-group text-center mt-4">
													<button className="btn btn-warning">Submit</button>
												</div>
											</div>
										</div>
									</form>
									{this.state.loader && this.state.loader &&
										<Loader />
									}
								</div>
							</div>
						</div>
					</div>
				</div>
				<AlertDialog
					open={this.state.setOpen}
					close={this.handleClose}
					status={this.state.status}
					message={this.state.message}
				/>
				<Footer />
			</>
		)
	}
}

export default ApplyIncubator