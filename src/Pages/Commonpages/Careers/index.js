import React, { useEffect, useState } from 'react'
import Footer from '../../../Container/Footer'
// import jobimage from '../../../Assets/img/'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { CurrentJobAction } from '../../../Action/careerAction'
import { Table } from '@material-ui/core'
import Axios from 'axios'
import { Api } from '../../../Const/Api'
import Carrerevent from '../../../Components/Carrerevent'
import CircularProgress from '@material-ui/core/CircularProgress';


const Career = ({ CurrentJobListState, CurrentJobListDispatch }) => {
    // console.log("CurrentJobListState==>>>", CurrentJobListState)

    const [jobtype, setJobtype] = useState('')
    const [contract, setContract] = useState("All")
    const [filter, setFilter] = useState(false)
    const [inputvalue, setInputvalue] = useState('')
    const [searchdata, setSearchdata] = useState('')
    const [loader, setLoader] = useState(true)
    const [errormessage, setErrormessage] = useState('')
    const [currentPage, setCurrentPage] = useState(1)


    useEffect(() => {
        document.getElementById("navTop").style.background = "#10152c";
        window.scrollTo(0, 0)
    }, [])

    useEffect(() => {
        let data = {
            currentPage: currentPage,
            searchdata: searchdata,
            contract: contract
        }
        CurrentJobListDispatch(data)
    }, [])

    useEffect(() => {
        setLoader(CurrentJobListState.loading)
        setErrormessage(CurrentJobListState.error)
    }, [CurrentJobListState.loading, CurrentJobListState.error])

    useEffect(() => {
        let data = {
            currentPage,
            searchdata,
            contract
        }
        CurrentJobListDispatch(data)
    }, [searchdata, contract])

    useEffect(() => {
        let data = {
            currentPage,
            searchdata,
            contract
        }
        CurrentJobListDispatch(data)
    }, [currentPage])

    useEffect(() => {
        Axios.get(Api.BaseUrl + "/api/contracttype-list")
            .then(resp => {
                const fetchjobtypeDAta = resp.data.data.list
                setJobtype(fetchjobtypeDAta)
            })
    }, [])

 
    const handleSearch = (e) => {
        setSearchdata(e.target.value)
        setCurrentPage(1)
    }

    const handleIncrement = (e) => {
        if (currentPage < pageCount) {
            setCurrentPage(currentPage + 1)
            CurrentJobListDispatch(currentPage + 1)
        }
    }
    const handlepaging = (pages) => {
        console.log("current page", pages)
        setCurrentPage(pages)
        CurrentJobListDispatch(pages)
    }


    const handleDecrement = (e) => {
        if (currentPage > 1) {
            setCurrentPage(currentPage - 1)
            CurrentJobListDispatch(currentPage - 1)
        }
    }

    let eventlist = []
    {
        CurrentJobListState.resp.length > 0 && CurrentJobListState.resp.map((event) => {
            let data = {
                image: event.logo_url,
                title: event.title,
                location: event.location,
                jobtype: event.job_type,
                sr_no: event.sr_no,
                company: event.company_name
            }
            eventlist.push(data)
        })
    }

    const pageCount = CurrentJobListState.pagination.pageCount
    const itemCount = CurrentJobListState.pagination.itemCount
    var paging = [];
    for (var i = 1; i < pageCount + 1; i++) {
        paging.push(i);
    }


    return (
        <>
            <div className="careers mt-73">
                <div className="careers-inner bg-careers parallax py-5">
                    <div className="container py-4 mb-md-5">
                        <div className="row">
                            <div className="col-12">
                                <h1 className="text-center text-white font-weight-bold mb-4">Join us for your dream job</h1>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <div className="search-job position-relative">
                                        <input type="text" className="form-control" placeholder="Search Job" onChange={handleSearch} />
                                        <i className="fas fa-search"></i>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <select className="form-control" value={contract} onChange={(e) => { setContract(e.target.value) }}>
                                        <option value="All">All</option>
                                        {jobtype.length > 0 && jobtype.map((job) => {
                                            return (
                                                <option value={job.contract_name}>{job.contract_name}</option>
                                            )
                                        })}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="searched-careers">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="searched-careers-inner d-flex flex-wrap mb-4">

                                    <Carrerevent
                                        length={itemCount}
                                        list={eventlist}
                                        loader={loader}
                                        errormessage={errormessage}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="pagination-main mt-2 mb-4 ">
                            <nav className={itemCount == 0 ? "d-none" : "mx-auto d-table"} aria-label="..." >
                                {console.log("itemCount", itemCount)}
                                {pageCount > 0 &&
                                    <ul className="pagination">
                                        
                                        <button disabled={currentPage == 1} className={currentPage == 1 ? "page-item disabled next-prev-btn" : "page-item next-prev-btn"} onClick={handleDecrement}>
                                            <span className="page-link"><i className="fas fa-angle-left" ></i> &nbsp; Previous</span>
                                        </button>
                                        {paging.length > 1 && paging.map(pages => {
                                            // let dis = currentPage === pages
                                            return (
                                                <button disabled={currentPage === pages} className={currentPage == pages ? "page-item active " : "page-item"} aria-current="page" onClick={() => handlepaging(pages)}>
                                                    <span className="page-link">{pages}<span className="sr-only">(current)</span></span>
                                                </button>
                                            )
                                        })}
                                        <button disabled={currentPage == pageCount} className={currentPage == pageCount ? "page-item disabled next-prev-btn" : "page-item next-prev-btn"} onClick={(e) => handleIncrement(e)}  >
                                            <a className="page-link" >Next &nbsp; <i className="fas fa-angle-right" ></i></a>
                                        </button>

                                    </ul>

                                }
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}
const mapStateToProps = state => {
    return {
        CurrentJobListState: state.CurrentJobReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        CurrentJobListDispatch: (data) => { dispatch(CurrentJobAction(data)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Career)