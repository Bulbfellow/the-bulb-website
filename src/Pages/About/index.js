import React from 'react'
// import techincubation from '../../Assets/img/tech_incubation.webp'
import Footer from '../../Container/Footer'
import AnchorLink from 'react-anchor-link-smooth-scroll'
import Imagecart from '../../Components/Imagecard'
import { team, advisoryteam, aboutdetails, aboutheading, vision, mission, Objective } from '../../Const/data'
import Aboutsection from '../../Components/Aboutsection'

class Aboutpage extends React.Component {
    constructor() {
        super()
        this.state = {
            currenttab: "whoweare",
        }
    }

    setCurrenttab = (name) => {
        this.setState({
            currenttab: name
        })
    }


    componentDidMount() {
        document.getElementById("navTop").style.background = "#10152c";
        window.addEventListener('scroll', this.handleScroll, true);
        window.scrollTo(0, 0)
    }
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    nav = React.createRef();

    handleScroll = () => {
        var lastScrollY = window.scrollY;
        if (lastScrollY > 1152) {
            this.setState({
                currenttab: 'team'
            })
        }
        if (lastScrollY > 2280) {
            this.setState({
                currenttab: 'advisory'
            })
        }
        if (lastScrollY < 1150) {
            this.setState({
                currenttab: 'whoweare'
            })
        }
    };

    render() {

        return (
            <>
                <div className="aboutus" ref={this.nav}>
                    {/* <div className="fixed-tab position-fixed">
                        <div onClick={() => this.setCurrenttab('whoweare')} className={`fix-tab ${this.state.currenttab === 'whoweare' && "active-tab"}`} >
                            <AnchorLink href='#whoweare'></AnchorLink>
                            <span className="tabtitle">Who we are</span>
                        </div>
                        <div onClick={() => this.setCurrenttab('team')} className={`fix-tab ${this.state.currenttab === 'team' && "active-tab"}`}>
                            <AnchorLink href='#ourTeam'></AnchorLink>
                            <span className="tabtitle">Our Team</span>
                        </div>
                        <div onClick={() => this.setCurrenttab('advisory')} className={`fix-tab ${this.state.currenttab === 'advisory' && "active-tab"}`} >
                            <AnchorLink href='#advisoryTeam'></AnchorLink>
                            <span className="tabtitle">Our advisory team</span>
                        </div>
                    </div> */}
                    <div className="aboutus_inner">
                        <section className="who-we-are" id="whoweare" >
                            <div className="whowe-bg parallax mb-4 pb-5">
                                <div className="container d-flex align-items-end h-100">
                                    <h1 className="banner-title">Who we are</h1>
                                </div>
                            </div>
                            <div className="tech_incubation py-5">
                                <div className="container">
                                    <div className="row align-items-center mb-5">
                                        <div className="col-md-5">
                                            <div className="inner">
                                                <h2 className="mb-4">{aboutheading}</h2>
                                                <p>{aboutdetails}</p>
                                            </div>
                                        </div>
                                        <div className="col-md-1"></div>
                                        <div className="col-md-6">
                                            <div className="imgright d-none d-md-block">
                                                <img src='https://thebulb.africa/Images/individual/tech_incubation.jpg' alt="tech_incubation" className="img-fluid w-100" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="mission-vision row">
                                        <Aboutsection heading={"Our vision"} details={vision} />
                                        <Aboutsection heading={"Our Mission"} details={mission} />
                                        <Aboutsection heading={"Objective"} details={Objective} />
                                    </div>
                                </div>
                            </div>
                        </section>

                        {/* <section className="our-team" id="ourTeam" >
                            <div className="team-bg parallax mb-4 pb-5">
                                <div className="container d-flex align-items-end h-100">
                                    <h1 className="banner-title">Our Team</h1>
                                </div>
                            </div>
                            <Imagecart team={team} />
                        </section> */}

                        {/* <section className="advisory-team" id="advisoryTeam" >
                            <div className="advisteam-bg parallax mb-4 pb-5">
                                <div className="container d-flex align-items-end h-100">
                                    <h1 className="banner-title">Our advisory team</h1>
                                </div>
                            </div>
                            <Imagecart team={advisoryteam} />
                        </section> */}

                    </div>
                </div>
                <Footer />
            </>
        )
    }
}

export default Aboutpage
