import React, { useEffect, useState } from 'react'
import Event from '../../Container/Newsfeedblock/Event'
import Gallary from '../../Container/Newsfeedblock/Gallary'
import Live from '../../Container/Newsfeedblock/Live'
import Recent from '../../Container/Newsfeedblock/Recent'
import Press from '../../Container/Newsfeedblock/Press'
import Tag from '../../Container/Newsfeedblock/Tag'
import Footer from '../../Container/Footer'


const Newsfeed = ({ calenderselect }) => {
  const [recent, setRecent] = useState(calenderselect === true ? 'event' : 'recent')

  useEffect(() => {
    document.getElementById("navTop").style.background = "#10152c";
    window.scrollTo(0, 0)
  }, [])



  return (
    <>
      <div className="newsfeed mt-73 middleContent" >
        <div className="newsfeed-inner py-5">
          <div className="container">

            <ul className="newsfeed-tabs nav">
              <li className="nav-item" onClick={() => setRecent('recent')}>
                <span className={`nav-link ${recent === 'recent' && "active"}`}  >Recent</span>
              </li>
              <li className="nav-item" onClick={() => setRecent('event')}>
                <span className={`nav-link ${recent === 'event' && "active"}`}  >Event</span>
              </li>
              <li className="nav-item" onClick={() => setRecent('tag')}>
                <span className={`nav-link ${recent === 'tag' && "active"}`}  >Tag</span>
              </li>
              <li className="nav-item" onClick={() => setRecent('press')}>
                <span className={`nav-link ${recent === 'press' && "active"}`}  >press</span>
              </li>
              <li className="nav-item" onClick={() => setRecent('live')}>
                <span className={`nav-link ${recent === 'live' && "active"}`}  >Live</span>
              </li>
              <li className="nav-item" onClick={() => setRecent('gallary')}>
                <span className={`nav-link ${recent === 'gallary' && "active"}`}  >Gallery</span>
              </li>
            </ul>
            <div className="tab-content" id="myTabContent">
              <div className="tab-pane fade show active " role="tabpanel" style={{marginTop:'50px'}}>
                {recent === 'recent' && "No post yet"
                  // <Recent />
                }
                {recent === 'event' &&
                //  "No posts yet"
                  <Event onClick={() => setRecent('gallary')} 
                  // calenderselect={calenderselect} 
                  />
                }
                {recent === 'tag' && 
                  <Tag />
                }
                {recent === 'live' && "No post yet"
                  // <Live />
                }
                {recent === 'gallary' &&
                //  "No post yet"
                  <Gallary />
                }
                {recent === 'press' &&
                  <Press />
                }
              </div>
            </div>
          </div>
        </div>
        {/* 
                <Switch>
                    <Route path={`${match.path}/:id`} component={Recentpost} />
                </Switch> */}

      </div>
      <Footer />
    </>
  )

}
export default Newsfeed

