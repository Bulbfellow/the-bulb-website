
import React, { useEffect, useState } from 'react'
import Footer from '../../Container/Footer'
import a from '../../Assets/img/coworkspace/desk.png'
import e from '../../Assets/img/coworkspace/wifi.png'
import { WorkspaceAction } from '../../Action/workspace'
import Slider from "react-slick";
import { connect } from 'react-redux'
import CircularProgress from '@material-ui/core/CircularProgress';

const Coworkspace = ({ CoworkSpaceState, CoworkSpaceDispatch }) => {
    console.log("CoworkSpaceState", CoworkSpaceState)
    const [loader, setLoader] = useState(false)
    const [errormessage, setErrormessage] = useState('')


    useEffect(() => {
        CoworkSpaceDispatch()
        document.getElementById("navTop").style.background = "#10152c";
        window.scrollTo(0, 0)
    }, [])

    useEffect(() => {
        setLoader(CoworkSpaceState.loading)
        setErrormessage(CoworkSpaceState.error)
    }, [CoworkSpaceState.loading, CoworkSpaceState.error])

    const settings = {
        className: "center",
        centerMode: true,
        infinite: false,
        slidesToShow: 3,
        speed: 500,
        dots: true,
        initialSlide: 2,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 2
                }
            }
        ]
    };

    return (
        <>
            {console.log("loader=............", errormessage)}
            <div className="co-workspace mt-73 py-5">
                <div className="container">
                    <h2 className="heading text-center d-none d-sm-block">Explore our facilities</h2>
                    <div className="co-workspace-inner py-2 py-md-4 mb-4">
                        <Slider{...settings} >
                            {loader && loader && <div className="loadercuss"><CircularProgress /></div>}
                            {errormessage}
                            {CoworkSpaceState.resp.map((workspace) => {
                                return (
                                    <div className="custom-slide">
                                        <img src={workspace.image_url} alt="sliderimage" className="img-fluid" />
                                        <div className="img-text-inner">
                                            <h3>{workspace.title}</h3>
                                            <p dangerouslySetInnerHTML={{ __html: workspace.description }} />
                                        </div>
                                    </div>
                                )
                            })}

                            {CoworkSpaceState.resp.length > 0 &&
                                <div className="custom-slide last-second disabled">
                                    <img src={e} alt="slider" className="img-fluid" />
                                    <div className="img-text-inner">
                                        <h3>1 Discussion area</h3>
                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod</p>
                                    </div>
                                </div>
                            }
                            {CoworkSpaceState.resp.length > 0 &&
                                <div className="custom-slide last disabled">
                                    <img src={a} alt="slider" alt="sliderimage" className="img-fluid" />
                                    <div className="img-text-inner">
                                        <h3>1 Discussion area</h3>
                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod</p>
                                    </div>
                                </div>
                            }
                        </Slider>
                    </div>
                    <div className="co-work-btn text-center my-3">
                        <a href="http://desk.africa/" target="_blank" className="d-inline-flex justify-content-center text-decoration-none">
                            <button className="btn btn-warning">Find out more <i className="ml-2 fas fa-chevron-circle-right"></i></button>
                        </a>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}
const mapStateToProps = state => {
    return {
        CoworkSpaceState: state.WorkspaceReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        CoworkSpaceDispatch: () => { dispatch(WorkspaceAction()) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Coworkspace)

