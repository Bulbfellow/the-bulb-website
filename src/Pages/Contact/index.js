import React, { useEffect } from 'react'
import Footer from '../../Container/Footer'
import Contactblock from '../../Components/Contactarea'
import Contactmap from '../../Components/Contactmap'

const Contact = () => {
    useEffect(() => {       
        document.getElementById("navTop").style.background = "#10152c";
    }, [])

    return (
        <>
            <div className="contactus-Main">
                <div className="contact-inner">
                    <div className="contact-first bg-light-gray py-5">
                        <div className="container">
                            <div className="row align-items-center">
                                <div className="col-md-6">
                                    <Contactmap
                                        addressheading={"Our address"}
                                        houseno={"39 Ikorodu Road,"}
                                        address={"Jibowu, Yaba, Lagos"}
                                        country={"Nigeria"}
                                        operation={"Hours of operation"}
                                        timing={"7 a.m. - 9 p.m."}
                                        day={"Monday - Sunday"}
                                        button={true}
                                    />
                                </div>
                                <div className="col-md-6">
                                    <div className="googlemap">
                                    <iframe title="map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15856.003339599858!2d3.3673051!3d6.5215752!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x61e31411b1bc213b!2sThe%20Bulb%20Africa!5e0!3m2!1sen!2sng!4v1580788765203!5m2!1sen!2sng" width="100%" height="350" frameBorder="0"  allowFullScreen=""></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div className="contact-third bg-light-gray py-5">
                        <div className="container pt-3">
                            <div className="row">
                                <div className="col-md-4">
                                    <Contactblock
                                        name={"Name goes here"}
                                        position={"Position goes here"}
                                        contact={"+234 700 843 2852"}
                                        mail={"info@thebulb.africa"}
                                        heading={true}
                                        headingname={"General contact info"}
                                        content={true}
                                    />
                                </div>
                                <div className="col-md-4">
                                    <Contactblock
                                        heading={true}
                                        headingname={"Mailing address"}
                                        mailing={true}
                                       position1={"The Bulb Africa"}
                                       address={"39 Ikorodu Road, Jibowu, Yaba,Lagos, Nigeria 100252"}
                                    />
                                </div>
                                <div className="col-md-4">
                                    <Contactblock
                                        // name={"The Bulb Africa"}
                                        // position={"Human Resources Team"}
                                        contact={"+234 906 200 0305"}
                                        mail={"careers@thebulb.africa"}
                                        headingname={"Careers contact"}
                                        heading={true}
                                    />
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}
export default Contact

