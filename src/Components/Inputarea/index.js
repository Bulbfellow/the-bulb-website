import React from 'react'

const Inputcomponents = ({ divclassname, label, Errorname, errormsg, type, HandleChange ,name,value }) => {
    return (
        <div className={divclassname}>
            <div className="form-group">
                <label htmlFor="organization">{label} {Errorname == errormsg ? <span className="errorMsg">*</span> : <span>*</span>}</label>
                <input type={type} onChange={HandleChange} value={value} className="form-control"  name={name}/>
                <div className="errorMsg">
                    {Errorname}
                </div>
            </div>
        </div>
    )
}
export default Inputcomponents