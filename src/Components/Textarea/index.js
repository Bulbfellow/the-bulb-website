import React from 'react'
import {connect} from 'react-redux'
const Textarea = ({ label, maxlength, rows, dataKey, valueChange ,state}) => {
    return (
        <div className="form-group">
            <label for="aboutYourself">{label}</label>
            <textarea
                className="form-control"
                maxlength={maxlength}
                rows={rows}
                value={state.Form && state.Form[dataKey]}
                onChange={(d) => valueChange({ key: dataKey, value: d.target.value })}>
            </textarea>
        </div>
    )
}
const mapStateToProps = state => {
    return {
        state
    }
}
const mapDispatchToProps = dispatch => {
    return {
        valueChange: d => { console.log("valueChange", d); dispatch({ type: d.key, payload: d.value }) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Textarea)

