import React from 'react'
import { connect } from 'react-redux'


const Input = ({ label, dataKey, state, valueChange, type , className, labelclass }) => {
    return (
        <div className="form-group">
            <label className={labelclass} for={{ label }} >{label}</label>
            {/* {JSON.stringify(state.Form)} */}
            <input className={className}
                type={type}
                value={state.Form && state.Form[dataKey]}
                onChange={d => valueChange({ key: dataKey, value: d.target.value })}
            />
        </div>
    )
}

const mapStateToProps = state => {
    return {
        state
    }
}
const mapDispatchToProps = dispatch => {
    return {
        valueChange: d => { console.log("valueChange", d); dispatch({ type: d.key, payload: d.value }) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Input)





// import React from 'react'

// const Inputtype = ({ label, type, id, min, max,onChange }) => {
//     return (
//         <div className="form-group">
//             <label for={{ label }}>{label}</label>
//             <input type={type} className="form-control" min={min} max={max} id={id} onChange={onChange} />
//         </div>
//     )
// }
// export default Inputtype
