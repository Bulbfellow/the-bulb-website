import React from 'react'
import { connect } from 'react-redux'
const statelist = ['Andaman and Nicobar Islands', 'Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar', 'Chandigarh', 'Chhattisgarh', 'Dadra and Nagar Haveli', 'Daman and Diu', 'Delhi',
    'Goa',
    'Gujarat',
    'Haryana',
    'Himachal Pradesh',
    'Jammu and Kashmir',
    'Jharkhand',
    'Karnataka',
    'Kerala',
    'Lakshadweep',
    'Madhya Pradesh',
    'Maharashtra',
    'Manipur',
    'Meghalaya',
    'Mizoram',
    'Nagaland',
    'Orissa',
    'Pondicherry',
    'Punjab',
    'Rajasthan',
    'Sikkim',
    'Tamil Nadu',
    'Telangana',
    'Tripura',
    'Uttaranchal',
    'Uttar Pradesh',
    'West Bengal']
const Select = ({ label, dataKey, state, valueChange }) => {
    return (
        <div className="form-group">
            <label for="state">{label}</label>
            <select
                name=""
                className="form-control"
                onChange={(d) => valueChange({ key: dataKey, value: d.target.value })}
                value={state.Form && state.Form[dataKey]}
            >
                {statelist.map((state) => {
                    return <option value={state}>{state}</option>
                })}
            </select>
        </div>
    )
}
const mapStateToProps = state => {
    return {
        state
    }
}
const mapDispatchToProps = dispatch => {
    return {
        valueChange: d => { console.log("valueChange", d); dispatch({ type: d.key, payload: d.value }) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Select)

// export default Select


// import React from 'react'

// const Selectoption = ({ }) => {
//     return (
//         <div className="form-group">
//             <label for="state">State</label>
//             <select name="" id="state" className="form-control">
//                 <option value="" selected>-select-</option>
//                 <option value="">Delhi</option>
//                 <option value="">Punjab</option>
//                 <option value="">Haryana</option>
//             </select>
//         </div>
//     )
// }
// export default Selectoption
