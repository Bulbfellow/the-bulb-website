import React from 'react'

const Aboutsection = ({ heading, details }) => {
    return (
        <div className="col-12 mb-4">
            <h3>{heading}</h3>
            <p>{details}</p>
        </div>
    )

}
export default Aboutsection
