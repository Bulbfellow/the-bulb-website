import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import { Dateconvert } from '../../Const/data'

const Newsfeedcard = ({ list, content, category, link }) => {
    return list.map((user) => {
        let data = user.description;
        let sub = data.substring(0, 130) + "...";
        return (
            <div key={user.id} className="col-md-4 col-sm-6" style={{ cursor: 'pointer' }}>
                <Link to={`${link}/${user.id}`} className="feed-section">
                    <div className="feed-section">
                        <div className="feedImg">
                            <img src={user.image} alt="Recent" className="img-fluid" />
                        </div>
                        <div className="feedContent">
                            <h3>{user.title}</h3>
                            {content && content ? '' :
                                data.length < 130 ?
                                    <span dangerouslySetInnerHTML={{ __html: user.description }}></span>
                                    : <span dangerouslySetInnerHTML={{ __html: sub }}></span>
                            }
                            <p className="date">{Dateconvert(user.date, 4)} <span className="text-warning">{category && category ? user.category : user.type}</span></p>
                        </div>
                    </div>
                </Link>
            </div>
        )
    })
}
export default withRouter(Newsfeedcard)
