import React from "react";
import Eventnofound from "../../Assets/img/no-found-events.svg";
import { Dateconvert } from '../../Const/data'

const Calenderevent = ({ list }) => {
    return (
        <>
            <div className="grid-view grid-events">
                <hr />
                {list.map((user) => {
                     let data = user.des;
                     let sub = data.substring(0, 5000) + "...";
                    return (
                        <div className="grid-events-inner py-4">
                            <div className="card mb-4 flex-row align-items-center">
                                <div className="card-left events-photo">
                                    <img src={user.image} alt="Events Image" style={{objectFit: "fill"}} className="img-fluid" />
                                </div>
                                <div className="card-right events-desc">
                                    <h4 className="event-title mb-0"><a href="#">{user.title}</a></h4>
                                {    data.length < 5000 ?
                                     <p className="event-des" dangerouslySetInnerHTML={{ __html: user.des }}></p>
                                    : <p className="event-des" dangerouslySetInnerHTML={{ __html: sub }}></p>}
                                    <div className="events-dateTime"><span className="date">{Dateconvert(user.date, 2)}</span>
                                        {/* . <span className="time">{user.time}</span> */}
                                    </div>
                                    <div className="events-place"> <span className="events-address">{user.address}</span></div>
                                    <div className="events-place"><span>{user.type}</span> </div>
                                    <div className="register-event mt-3 text-right">
                                        <a className="btn btn-outline-warning" target="_blank" href={user.registerlink}>Register</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
            {/* <div className="no-event">
            <div className="row align-items-center">
                <div className="col-md-6">
                    <h3 className="font-weight-bold section-heading">Upcoming event</h3>
                    <p className="py-2 pr-md-5">You have no upcoming events. Incase you missed previous events, you can check them out</p>
                    <button className="btn btn-outline-warning px-5">Explore</button>
                </div>
                <div className="col-md-6">
                    <img src={Eventnofound} alt="No Event Found" className="img-fluid mx-auto" />
                </div>
            </div>
        </div> */}
        </>
    )
}
export default Calenderevent
