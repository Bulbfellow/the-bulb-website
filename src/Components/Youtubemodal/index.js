import React from 'react';
import Dialog from '@material-ui/core/Dialog';


const Youtubmodal = ({ open, close }) => {
    return (
        <div>
            <Dialog
                className="thankyou"
                open={open}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <botton onClick={close}>Close</botton>
                <iframe width="100%" height="430" src="https://www.youtube.com/embed/AjkYTJklAa8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen style={{ borderRadius: '5px' }}></iframe>

            </Dialog>
        </div>
    );
}
export default Youtubmodal