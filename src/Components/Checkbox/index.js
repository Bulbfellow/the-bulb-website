import React from 'react'
import { connect } from 'react-redux'

const CheckboxInput = ({ label, dataKey, state, valueChange, type, className, labelclass, valuee }) => {
    return (
        <div className="form-group">
            <label className={labelclass} for={{ label }} >{label}</label>
            <input className={className}
                type={type}
                value={state.Form && state.Form[dataKey]}
                onChange={d => valueChange({ key: dataKey, value: d.target.checked })}
            defaultChecked={valuee}
            />
        </div>
    )
}
const mapStateToProps = state => {
    return {
        state
    }
}
const mapDispatchToProps = dispatch => {
    return {
        valueChange: d => { console.log("valueChange", d); dispatch({ type: d.key, payload: d.value }) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckboxInput)

