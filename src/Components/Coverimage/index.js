import React from 'react'

const Coverimage = ({ image, name }) => {
    return (
        <div>
            <img src={image} alt="dd" className="" />
            <h3>{name}</h3>
        </div>
    )
}
export default Coverimage
