import React from 'react'

const Upcoming = ({ list }) => {
    return (
        <>
            {
                list.map((user) => {
                    return (
                        <div className="col-md-4 col-sm-6">
                            <div className="events-inner">
                                <a href="#">
                                    <div className="events-Img">
                                        <img src={user.image} alt="Upcoming Events" className="img-fluid" />
                                    </div>
                                    <div className="eventsContent">
                                        <p className="date">{user.date}</p>
                                        <h5>{user.title}</h5>
                                        <p className="place">{user.place}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    )
                })
            }
        </>
    )
}
export default Upcoming