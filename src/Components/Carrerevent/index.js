import React from 'react'
import { Link } from 'react-router-dom'
import CircularProgress from '@material-ui/core/CircularProgress';

const Carrerevent = ({length, list, loader, errormessage }) => {
    return (
        <>
            {loader && loader && <CircularProgress />}
            <h5 className="job-found-title w-100 flex-grow-1 text-center py-4 mb-5">{length === 0 ? "No" : length} <span>Jobs found</span></h5>
            {errormessage}
            {list.map((user) => {
                return (
                    <div className="founded-jobs d-flex flex-wrap justify-content-between" >
                        <div className="founded-jobs-inner d-flex flex-wrap align-items-center">
                            <h4 className="flex-grow-1 w-100">{user.title}</h4>
                            <div className="founded-jobs-img">
                                <img src={user.image} alt="job image" className="joblistimage img-fluid" />
                            </div>
                            <div className="founded-jobs-dtl d-flex align-items-center justify-content-between">
                                <div className="job-title">
                                    <h5>{user.company}</h5>
                                    <p><i className="fas fa-briefcase"></i>{user.jobtype}</p>
                                </div>
                            </div>
                            <div className="right-icon">
                                <Link to={`/jobs/${user.sr_no}`}><i className="fas fa-angle-right"></i></Link>
                            </div>
                        </div>
                    </div >
                )
            })
            }
        </>
    )
}
export default Carrerevent