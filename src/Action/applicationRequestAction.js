import * as ACTION_TYPE from './actiontype'
import Axios from 'axios'
import { Api } from '../Const/Api'

export const SubmitIdeaSuccess = payload => {
    return {
        type: ACTION_TYPE.SUBMIT_IDEA_SUCCESS,
        payload
    }
}
export const SubmitIdeaLoading = () => {
    return {
        type: ACTION_TYPE.SUBMIT_IDEA_LOADING,
    }
}
export const SubmitIdeaError = payload => {
    return {
        type: ACTION_TYPE.SUBMIT_IDEA_ERROR,
        payload
    }
}

export const SubmitIdeaAction = (data) => {
    return dispatch => {
        Axios.post(Api.BaseUrl + "/api/submitidea", data)
            .then(resp => {
                // console.log("res", resp)
                dispatch(SubmitIdeaLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(SubmitIdeaSuccess(resp.data));
                    }
                    if (resp.data.status === "0") {
                        dispatch(SubmitIdeaError(resp.data.message));
                    }
                } else {
                    dispatch(SubmitIdeaError(resp.data.message));
                }
            })
            .catch(err => {
                dispatch(SubmitIdeaError(err.message));
            });
    };
};

export const IncubatorSuccess = payload => {
    return {
        type: ACTION_TYPE.INCUBATOR_SUCCESS,
        payload
    }
}
export const IncubatorLoading = () => {
    return {
        type: ACTION_TYPE.INCUBATOR_LOADING,
    }
}
export const IncubatorError = payload => {
    return {
        type: ACTION_TYPE.INCUBATOR_ERROR,
        payload
    }
}


export const IncubationAction = (data) => {
    // console.log("datadatadata=>>>>>", data)
    return dispatch => {
        Axios.post(Api.BaseUrl + "/api/applyforincubator", data)
            .then(resp => {
                // console.log("res", resp)
                dispatch(IncubatorLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(IncubatorSuccess(resp.data));
                    }
                    if (resp.data.status === "0") {
                        dispatch(IncubatorError(resp.data.message));
                    }
                } else {
                    dispatch(IncubatorError(resp.data.message));
                }
            })
            .catch(err => {
                dispatch(IncubatorError(err.message));
            });
    };
};

export const HiredeveloperSuccess = payload => {
    return {
        type: ACTION_TYPE.HIRE_DEVELOPER_SUCCESS,
        payload
    }
}
export const HiredeveloperLoading = () => {
    return {
        type: ACTION_TYPE.HIRE_DEVELOPER_LOADING,
    }
}
export const HiredeveloperError = payload => {
    return {
        type: ACTION_TYPE.HIRE_DEVELOPER_ERROR,
        payload
    }
}

export const HireDeveloperAction = (data) => {
    // console.log("datadatadata=>>>>>", data)
    return dispatch => {
        Axios.post(Api.BaseUrl + "/api/hire", data)
            .then(resp => {
                // console.log("res", resp)
                dispatch(HiredeveloperLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(HiredeveloperSuccess(resp.data));
                    }
                    if (resp.data.status === "0") {
                        dispatch(HiredeveloperError(resp.data.message));
                    }
                } else {
                    dispatch(HiredeveloperError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(HiredeveloperError(err.message));
            });
    };
};

export const RequesttourSuccess = payload => {
    return {
        type: ACTION_TYPE.REQUEST_TOUR_SUCCESS,
        payload
    }
}
export const RequesttourLoading = () => {
    return {
        type: ACTION_TYPE.REQUEST_TOUR_LOADING,
    }
}
export const RequesttourError = payload => {
    return {
        type: ACTION_TYPE.REQUEST_TOUR_ERROR,
        payload
    }
}

export const RequesttourAction = (data) => {
    // console.log("datadatadata=>>>>>", data)
    return dispatch => {
        Axios.post(Api.BaseUrl + "​/api/requesttour", data)
            .then(resp => {
                // console.log("res", resp)
                dispatch(RequesttourLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(RequesttourSuccess(resp.data));
                    }
                    if (resp.data.status === "0") {
                        dispatch(RequesttourError(resp.data.message));
                    }
                } else {
                    dispatch(RequesttourError(resp.data.message));
                }
            })
            .catch(err => {
                dispatch(RequesttourError(err.message));
            });
    };
};

export const PartnershipSuccess = payload => {
    return {
        type: ACTION_TYPE.PARTNERSHIP_SUCCESS,
        payload
    }
}
export const PartnershipLoading = () => {
    return {
        type: ACTION_TYPE.PARTNERSHIP_LOADING,
    }
}
export const PartnershipError = payload => {
    return {
        type: ACTION_TYPE.PARTNERSHIP_ERROR,
        payload
    }
}

export const PartnershipAction = (data) => {
    // console.log("datadatadata=>>>>>", data)
    return dispatch => {
        Axios.post(Api.BaseUrl + "/api/partnership", data)
            .then(resp => {
                // console.log("res", resp)
                dispatch(PartnershipLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(PartnershipSuccess(resp.data));
                    }
                    if (resp.data.status === "0") {
                        dispatch(PartnershipError(resp.data.message));
                    }
                } else {
                    dispatch(PartnershipError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(PartnershipError(err.message));
            });
    };
};

export const ScheduleSuccess = payload => {
    return {
        type: ACTION_TYPE.SCHEDULE_VISIT_SUCCESS,
        payload
    }
}
export const ScheduleLoading = () => {
    return {
        type: ACTION_TYPE.SCHEDULE_VISIT_LOADING,
    }
}
export const ScheduleError = payload => {
    return {
        type: ACTION_TYPE.SCHEDULE_VISIT_ERROR,
        payload
    }
}

export const SchedulevisitAction = (data) => {
    // console.log("datadatadata=>>>>>", data)
    return dispatch => {
        Axios.post(Api.BaseUrl + "/api/schedulevisit", data)
            .then(resp => {
                // console.log("res", resp)
                dispatch(ScheduleLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(ScheduleSuccess(resp.data));
                    }
                    if (resp.data.status === "0") {
                        dispatch(ScheduleError(resp.data.message));
                    }
                } else {
                    dispatch(ScheduleError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(ScheduleError(err.message));
            });
    };
};