import * as ACTION_TYPE from './actiontype'
import Axios from 'axios'
import { Api } from '../Const/Api'

export const CurrentJobSuccess = payload => {
    return {
        type: ACTION_TYPE.CURRENT_JOB_VACANCY_SUCCESS,
        payload
    }
}
export const CurrentJobLoading = () => {
    return {
        type: ACTION_TYPE.CURRENT_JOB_VACANCY_LOADING,
    }
}
export const CurrentJobError = payload => {
    return {
        type: ACTION_TYPE.CURRENT_JOB_VACANCY_ERROR,
        payload
    }
}
export const CurrentJobCountSuccess = payload => {
    return {
        type: ACTION_TYPE.CURRENT_JOB_VACANCY_COUNT_SUCCESS,
        payload
    }
}


export const CurrentJobAction = (data) => {
    // console.log("data comming", data)
    return dispatch => {
        Axios.get(Api.BaseUrl + `/api/careerpostbytitle/?text=${data.searchdata}&type=${data.contract}&limit=10&page=` + data.currentPage)
            .then(resp => {
                // console.log("res", resp)
                dispatch(JobFromSubmitLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(CurrentJobSuccess(resp.data.data.list));
                        dispatch(CurrentJobCountSuccess(resp.data.data));
                    }
                    if (resp.data.status === "0") {
                        dispatch(CurrentJobError(resp.data.message));
                    }
                } else {
                    dispatch(CurrentJobError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(CurrentJobError(err.message));
                dispatch(CurrentJobError("we are unable to fetch your data Please check your internet connection or try again"));
            });
    };
};

export const JobFromSubmitSuccess = payload => {
    return {
        type: ACTION_TYPE.JOB_FORM_SUBMIT_SUCCESS,
        payload
    }
}
export const JobFromSubmitLoading = () => {
    return {
        type: ACTION_TYPE.JOB_FORM_SUBMIT_LOADING,
    }
}
export const JobFromSubmitError = payload => {
    return {
        type: ACTION_TYPE.JOB_FORM_SUBMIT_ERROR,
        payload
    }
}

export const JobFormAction = (data) => {
    return dispatch => {
        Axios.post(Api.BaseUrl + "/api/career", data)
            .then(resp => {
                // console.log("res", resp)
                dispatch(JobFromSubmitLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(JobFromSubmitSuccess(resp.data));
                    }
                    if (resp.data.status === "0") {
                        dispatch(JobFromSubmitError(resp.data.message));
                    }
                } else {
                    dispatch(JobFromSubmitError(resp.data.message));
                }
            })
            .catch(err => {
                dispatch(JobFromSubmitError(err.message));
            });
    };
};
