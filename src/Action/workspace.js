import * as ACTION_TYPE from './actiontype'
import Axios from 'axios'
import { Api } from '../Const/Api'

export const WorkspaceSuccess = payload => {
    return {
        type: ACTION_TYPE.WORK_SPACE_SUCCESS,
        payload
    }
}
export const WorkspaceLoading = () => {
    return {
        type: ACTION_TYPE.WORK_SPACE_LOADING,
    }
}
export const WorkspaceError = payload => {
    return {
        type: ACTION_TYPE.WORK_SPACE_ERROR,
        payload
    }
}

export const WorkspaceAction = () => {
    return dispatch => {
        Axios.get(Api.BaseUrl + "/api/workspace")
            .then(resp => {
                console.log("res",resp)
                dispatch(WorkspaceLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(WorkspaceSuccess(resp.data.data.list));
                    }
                    if (resp.data.status === "0") {
                        dispatch(WorkspaceError(resp.data.message));
                    }
                } else {
                    dispatch(WorkspaceError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(WorkspaceError(err.message));
            });
    };
};