import * as ACTION_TYPE from './actiontype'
import Axios from 'axios'
import { Api } from '../Const/Api'

export const StateSuccess = payload => {
    return {
        type: ACTION_TYPE.STATE_SUCCESS,
        payload
    }
}
export const StateLoading = () => {
    return {
        type: ACTION_TYPE.STATE_LOADING,
    }
}
export const StateError = payload => {
    return {
        type: ACTION_TYPE.STATE_ERROR,
        payload
    }
}

export const State = () => {
    return dispatch => {
        Axios.get(Api.BaseUrl + "/api/states")
            .then(resp => {
                dispatch(StateLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(StateSuccess(resp.data.data.list));
                    }
                    if (resp.data.status === "0") {
                        dispatch(StateError(resp.data.message));
                    }
                } else {
                    dispatch(StateError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(StateError(err.message));
            });
    };
};

export const CitySuccess = payload => {
    return {
        type: ACTION_TYPE.CITY_SUCCESS,
        payload
    }
}
export const CityLoading = () => {
    return {
        type: ACTION_TYPE.CITY_LOADING,
    }
}
export const CityError = payload => {
    return {
        type: ACTION_TYPE.CITY_ERROR,
        payload
    }
}

export const City = (id) => {
    return dispatch => {
        Axios.get(Api.BaseUrl+'/api/states/' + id)
            .then(resp => {
                // console.log("city list", resp)
                dispatch(CityLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(CitySuccess(resp.data.data.list));
                    }
                    if (resp.data.status === "0") {
                        dispatch(CityError(resp.data.message));
                    }
                } else {
                    dispatch(CityError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(CityError(err.message));
            });
    };
};

export const UploadfileSuccess = payload => {
    return {
        type: ACTION_TYPE.UPLOAD_FILE_SUCCESS,
        payload
    }
}
export const UploadfileLoading = () => {
    return {
        type: ACTION_TYPE.UPLOAD_FILE_LOADING,
    }
}
export const UploadfileError = payload => {
    return {
        type: ACTION_TYPE.UPLOAD_FILE_ERROR,
        payload
    }
}

export const Uploadfile = (file) => {
    let formData = new FormData()
    formData.append(
        'file',
        file
    )
    return dispatch => {
        Axios.post(Api.BaseUrl+'/api/file-upload' , formData)
            .then(resp => {
                console.log("resp======file",resp)
                dispatch(StateLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(UploadfileSuccess(resp.data.data.image_url));
                    }
                    if (resp.data.status === "0") {
                        dispatch(UploadfileError(resp.data.message));
                    }
                } else {
                    dispatch(UploadfileError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(UploadfileError(err.message));
            });
    };
};
