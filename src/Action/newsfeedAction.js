import * as ACTION_TYPE from './actiontype'
import Axios from 'axios'
import { Api } from '../Const/Api'

export const NewsfeedEventSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_EVENT_SUCCESS,
        payload
    }
}
export const NewsfeedEventCountSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_EVENT_COUNT_SUCCESS,
        payload
    }
}

export const NewsfeedEventLoading = () => {
    return {
        type: ACTION_TYPE.NEWSFEED_EVENT_LOADING,
    }
}
export const NewsfeedEventError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_EVENT_ERROR,
        payload
    }
}
//-----------------------
export const NewsfeedEventCalenderSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_EVENT_CALENDER_SUCCESS,
        payload
    }
}


export const NewsfeedEventCalenderLoading = () => {
    return {
        type: ACTION_TYPE.NEWSFEED_EVENT_CALENDER_LOADING,
    }
}
export const NewsfeedEventCalenderError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_EVENT_CALENDER_ERROR,
        payload
    }
}

export const NewsFeedEventCalender = (dataLimit) => {
    // console.log("dataLimit",dataLimit)
    return dispatch => {
        Axios.get(Api.BaseUrl + `/api/events`)
            .then(resp => {
                // console.log("res", resp)
                dispatch(NewsfeedEventCalenderLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(NewsfeedEventCalenderSuccess(resp.data.data.list));
                    }
                    if (resp.data.status === "0") {
                        dispatch(NewsfeedEventCalenderError(resp.data.message));
                    }
                } else {
                    dispatch(NewsfeedEventCalenderError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(NewsfeedEventCalenderError(err.message));
            });
    };
};

export const NewsFeedEvent = (data) => {
    // console.log("data",data)
    return dispatch => {
        Axios.get(Api.BaseUrl + `/api/events?event_type=${data.search}&page=1&limit=${data.dataLimit}`)
            .then(resp => {
                // console.log("res", resp)
                dispatch(NewsfeedEventLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(NewsfeedEventSuccess(resp.data.data.list));
                        dispatch(NewsfeedEventCountSuccess(resp.data.data));
                    }
                    if (resp.data.status === "0") {
                        dispatch(NewsfeedEventError(resp.data.message));
                    }
                } else {
                    dispatch(NewsfeedEventError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(NewsfeedEventError(err.message));
            });
    };
};

export const NewsfeedTagSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_TAG_SUCCESS,
        payload
    }
}
export const NewsfeedTagLoading = () => {
    return {
        type: ACTION_TYPE.NEWSFEED_TAG_LOADING,
    }
}
export const NewsfeedTagError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_TAG_ERROR,
        payload
    }
}

export const NewsfeedTagCountSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_TAG_COUNT_SUCCESS,
        payload
    }
}



export const NewsFeedTag = (data) => {
    // console.log("data",data)
    return dispatch => {
        Axios.get(Api.BaseUrl + `/api/tags?tag_type=${data.search}&page=1&limit=${data.dataLimit}`)
            .then(resp => {
                // console.log("res", resp)
                dispatch(NewsfeedTagLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(NewsfeedTagSuccess(resp.data.data.list));
                        dispatch(NewsfeedTagCountSuccess(resp.data.data))
                    }
                    if (resp.data.status === "0") {
                        dispatch(NewsfeedTagError(resp.data.message));
                    }
                } else {
                    dispatch(NewsfeedTagError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(NewsfeedTagError(err.message));
            });
    };
};

export const NewsfeedPressSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_PRESS_SUCCESS,
        payload
    }
}
export const NewsfeedPressCountSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_PRESS_COUNT_SUCCESS,
        payload
    }
}

export const NewsfeedPressLoading = () => {
    return {
        type: ACTION_TYPE.NEWSFEED_PRESS_LOADING,
    }
}
export const NewsfeedPressError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_PRESS_ERROR,
        payload
    }
}

export const NewsfeedTagPress = (dataLimit) => {
    return dispatch => {
        Axios.get(Api.BaseUrl + `/api/presslist?limit=${dataLimit}&page=1`)
            .then(resp => {
                // console.log("res", resp)
                dispatch(NewsfeedPressLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(NewsfeedPressSuccess(resp.data.data.list));
                        dispatch(NewsfeedPressCountSuccess(resp.data.data))
                    }
                    if (resp.data.status === "0") {
                        dispatch(NewsfeedPressError(resp.data.message));
                    }
                } else {
                    dispatch(NewsfeedPressError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(NewsfeedPressError(err.message));
            });
    };
};

export const NewsfeedGallerySuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_GALLERY_SUCCESS,
        payload
    }
}
export const NewsfeedGalleryCountSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_GALLERY_COUNT_SUCCESS,
        payload
    }
}
export const NewsfeedGalleryLoading = () => {
    return {
        type: ACTION_TYPE.NEWSFEED_GALLERY_LOADING,
    }
}
export const NewsfeedGalleryError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_GALLERY_ERROR,
        payload
    }
}

export const NewsfeedTagGallery = (dataLimit) => {
    return dispatch => {
        Axios.get(Api.BaseUrl + `/api/gallerylist?limit=${dataLimit}&page=1`)
            .then(resp => {
                // console.log("res", resp)
                dispatch(NewsfeedGalleryLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(NewsfeedGallerySuccess(resp.data.data.list));
                        dispatch(NewsfeedGalleryCountSuccess(resp.data.data))
                    }
                    if (resp.data.status === "0") {
                        dispatch(NewsfeedGalleryError(resp.data.message));
                    }
                } else {
                    dispatch(NewsfeedGalleryError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(NewsfeedGalleryError(err.message));
            });
    };
};
